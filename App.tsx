import 'react-native-gesture-handler'
import { StatusBar } from 'expo-status-bar'
import RouteCenter from './src/routes/RouteCenter'
import { Provider } from 'react-redux'
import { store } from './src/redux/store'
import React from 'react'
import {
  useFonts,
  Inter_100Thin,
  Inter_200ExtraLight,
  Inter_300Light,
  Inter_400Regular,
  Inter_500Medium,
  Inter_600SemiBold,
  Inter_700Bold,
  Inter_800ExtraBold,
  Inter_900Black
} from '@expo-google-fonts/inter'

import { useQuery, useMutation, useQueryClient, QueryClient, QueryClientProvider } from '@tanstack/react-query'
const queryClient = new QueryClient()

export default function App() {
  let [fontsLoaded] = useFonts({
    Inter_100Thin,
    Inter_200ExtraLight,
    Inter_300Light,
    Inter_400Regular,
    Inter_500Medium,
    Inter_600SemiBold,
    Inter_700Bold,
    Inter_800ExtraBold,
    Inter_900Black
  })

  if (!fontsLoaded) {
    return null
  }

  return (
    <Provider store={store}>
      <QueryClientProvider client={queryClient}>
        <RouteCenter />
      </QueryClientProvider>
      <StatusBar style='auto' />
    </Provider>
  )
}
