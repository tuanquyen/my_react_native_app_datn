import { BottomTabNavigationEventMap } from '@react-navigation/bottom-tabs'
import { BottomTabDescriptorMap } from '@react-navigation/bottom-tabs/lib/typescript/src/types'
import { NavigationHelpers, ParamListBase, TabNavigationState } from '@react-navigation/native'
import { View, Text, TouchableOpacity } from 'react-native'

interface TabBarProps {
  navigation: NavigationHelpers<ParamListBase, BottomTabNavigationEventMap>
  state: TabNavigationState<ParamListBase>
  descriptors: BottomTabDescriptorMap
}

export default function TabBar({ state, descriptors, navigation }: TabBarProps) {
  return (
    <View style={{ flexDirection: 'row' }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key]

        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
              ? options.title
              : route.name

        const isFocused = state.index === index

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true
          })

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name, route.params)
          }
        }

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key
          })
        }

        return (
          <TouchableOpacity
            accessibilityRole='button'
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{ flex: 1 }}
          >
            {options.tabBarIcon && options.tabBarIcon({ focused: isFocused, color: 'black', size: 24 })}
            <Text style={{ color: isFocused ? '#673ab7' : '#222' }}>{label as string}</Text>
          </TouchableOpacity>
        )
      })}
    </View>
  )
}
