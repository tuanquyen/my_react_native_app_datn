import { NavigationContainer, NavigatorScreenParams } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import DemoScreen from '@/screens/OrderScreen'
import ResultScreen from '@/screens/ResultScreen'
import PaymentScreen from '@/screens/PaymentScreen'
import { TransferInfoType } from '@/types/types'
import AlertComponent from '@/components/Common/Alert/AlertComponent'
import { useDispatch, useSelector } from 'react-redux'
import BottomRoute from './BottomRoute'
import { BottomTabParamsList } from './BottomRoute'
import { defaultTheme, darkTheme } from '@/themes/themes'
import OnboardingScreen from '@/screens/OnboardingScreen'
import { PaperProvider } from 'react-native-paper'
import ProductScreen from '@/screens/ProductScreen'
import { SelectionType } from '@/redux/store'
import ProgressBarScreen from '@/screens/Demo/ProgressBar'
import GridListScreen from '@/screens/Demo/GridListScreen'
import ToastsScreen from '@/screens/Demo/ToastScreen'
import { useMemo } from 'react'
import { fetchCategories } from '@/redux/category'
import SignInScreen from '@/screens/SignInScreen'
import SignUpScreen from '@/screens/SignUpScreen'
import BagHasItemScreen from '@/screens/BagHasItemScreen'
import { Dialog, Incubator, ModalProps } from 'react-native-ui-lib'
import { gestureHandlerRootHOC } from 'react-native-gesture-handler'

const Stack = createStackNavigator<RootStackParamList>()

const { Toast } = Incubator

export type RootStackParamList = {
  Demo: undefined
  ProgressBar: undefined
  Onboarding: undefined
  Product: {
    product_id: number
  }
  BagHasItem: undefined
  SignIn: { email: string; password: string } | undefined
  SignUp: undefined
  BottomRoute: NavigatorScreenParams<BottomTabParamsList>
  Result: { orderCode: number }
  Payment: TransferInfoType
}
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import DrawerScreen from '@/screens/Demo/DrawerScreen'
import IncubatorDialogScreen from '@/screens/Demo/IncubatorDialogScreen'
import SkeletonViewScreen from '@/screens/Demo/SkeletonView'
import { fetchCart } from '@/redux/cart'

export type SignInProps = NativeStackScreenProps<RootStackParamList, 'SignIn'>
export type SignUpProps = NativeStackScreenProps<RootStackParamList, 'SignUp'>
export type BagHasItemProps = NativeStackScreenProps<RootStackParamList, 'BagHasItem'>
export type DemoProps = NativeStackScreenProps<RootStackParamList, 'Demo'>
export type ResultProps = NativeStackScreenProps<RootStackParamList, 'Result'>
export type PaymentProps = NativeStackScreenProps<RootStackParamList, 'Payment'>
export type BottomRouteProps = NativeStackScreenProps<RootStackParamList, 'BottomRoute'>
export type OnboardingProps = NativeStackScreenProps<RootStackParamList, 'Onboarding'>
export type ProductProps = NativeStackScreenProps<RootStackParamList, 'Product'>
export type ProgressBarProps = NativeStackScreenProps<RootStackParamList, 'ProgressBar'>

const RouteCenter = () => {
  const linking = {
    prefixes: ['payosdemoreact://'],
    config: {
      screens: {
        Demo: 'Demo',
        Result: 'Result',
        Payment: 'Payment',
        Onboarding: 'Onboarding',
        Product: 'Product',
        Test: 'ProgressBar',
        SignIn: 'SignIn',
        SignUp: 'SignUp',
        BagHasItem: 'BagHasItem',
        BottomRoute: {
          screens: {
            Home: 'Home',
            Category: 'Category',
            BagNoItem: 'BagNoItem',
            Setting: 'Setting',
            Wishlist: 'Wishlist'
          }
        },
        NotFound: '*'
      }
    }
  }
  const themes = useSelector((state: SelectionType) => state.themes)
  const toast = useSelector((state: SelectionType) => state.toast)
  const bottomDialog = useSelector((state: SelectionType) => state.bottomDialog)
  const centerDialog = useSelector((state: SelectionType) => state.centerDialog)

  const dispatch = useDispatch()
  useMemo(() => {
    dispatch(fetchCategories() as any)
    dispatch(fetchCart() as any)
  }, [])

  return (
    <PaperProvider theme={!themes ? defaultTheme : darkTheme}>
      <NavigationContainer theme={!themes ? defaultTheme : darkTheme} linking={linking as any}>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
            gestureEnabled: false
          }}
          initialRouteName='BottomRoute'
        >
          <Stack.Screen name='SignIn' component={SignInScreen} />
          <Stack.Screen name='SignUp' component={SignUpScreen} />
          <Stack.Screen name='BagHasItem' component={BagHasItemScreen} />
          <Stack.Screen name='Demo' component={DemoScreen} />
          <Stack.Screen name='Result' component={ResultScreen} />
          <Stack.Screen name='Payment' component={PaymentScreen} />
          <Stack.Screen name='BottomRoute' component={BottomRoute} />
          <Stack.Screen name='Onboarding' component={OnboardingScreen} />
          <Stack.Screen name='Product' component={ProductScreen} />
          <Stack.Screen name='ProgressBar' component={IncubatorDialogScreen} />
        </Stack.Navigator>
        <Toast {...toast} />
        <Incubator.Dialog {...bottomDialog} />
        <Dialog {...centerDialog} />
      </NavigationContainer>
    </PaperProvider>
  )
}

export default gestureHandlerRootHOC(RouteCenter)
