import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { BottomNavigation } from 'react-native-paper'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import HomeScreen from '@/screens/HomeScreen'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome6Icon from 'react-native-vector-icons/FontAwesome5'
import { StyleSheet } from 'react-native'
import { CommonActions } from '@react-navigation/native'
import CategoryScreen from '@/screens/CategoryScreen'
import AccountScreen from '@/screens/AccountScreen'
import WishlistScreen from '@/screens/WishlistScreen'
import { useAppTheme } from '@/hooks/useAppTheme'
import BagNoItemScreen from '@/screens/BagNoItemScreen'
import { BottomTabScreenProps } from '@react-navigation/bottom-tabs'
import useGetCart from '@/hooks/query/useGetCart'

const Tab = createBottomTabNavigator<BottomTabParamsList>()
export type BottomTabParamsList = {
  Home: undefined
  Category: undefined
  Wishlist: undefined
  Account: undefined
  BagNoItem: undefined
}
export type HomeProps = BottomTabScreenProps<BottomTabParamsList, 'Home'>
export type CategoryProps = BottomTabScreenProps<BottomTabParamsList, 'Category'>
export type WishlistProps = BottomTabScreenProps<BottomTabParamsList, 'Wishlist'>
export type AccountProps = BottomTabScreenProps<BottomTabParamsList, 'Account'>
export type BagNoItemProps = BottomTabScreenProps<BottomTabParamsList, 'BagNoItem'>

const BottomRoute = () => {
  const { colors } = useAppTheme()
  const { variants } = useGetCart()

  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false
      }}
      tabBar={({ navigation, state, descriptors, insets }) => (
        <BottomNavigation.Bar
          navigationState={state}
          safeAreaInsets={insets}
          shifting={true}
          getBadge={({ route }) => {
            if (route.name === 'Bag') {
              return 1
            }
          }}
          style={{
            backgroundColor: colors.background,
            borderTopColor: colors.tertiary,
            borderTopWidth: 0.5
          }}
          activeColor={colors.primary}
          inactiveColor={colors.secondary}
          onTabPress={({ route, preventDefault }) => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true
            })

            if (event.defaultPrevented) {
              preventDefault()
            } else {
              navigation.dispatch({
                ...CommonActions.navigate(route.name, route.params),
                target: state.key
              })
            }
          }}
          renderIcon={({ route, focused, color }) => {
            const { options } = descriptors[route.key]

            if (options.tabBarIcon) {
              return options.tabBarIcon({ focused, color, size: 24 })
            }
            return null
          }}
          theme={{ colors: { secondaryContainer: colors.tertiary } }}
          getLabelText={({ route }) => {
            const { options } = descriptors[route.key]
            const label =
              options.tabBarLabel !== undefined
                ? options.tabBarLabel.toString()
                : options.title !== undefined
                  ? options.title
                  : route.name
            return label
          }}
        />
      )}
    >
      <Tab.Screen
        name='Home'
        component={HomeScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => {
            return <Ionicons name='home' size={size} color={color} /> // home-outline
          }
        }}
      />
      <Tab.Screen
        name='Category'
        component={CategoryScreen}
        options={{
          tabBarLabel: 'Category',
          tabBarIcon: ({ color, size }) => {
            return <Ionicons name='list' size={size + 3} color={color} />
          }
        }}
      />
      <Tab.Screen
        name='BagNoItem'
        listeners={({ navigation }) => ({
          tabPress: (event) => {
            event.preventDefault()

            if (variants.length) {
              navigation.navigate('BagHasItem')
            } else {
              navigation.navigate('BagNoItem')
            }
          }
        })}
        component={BagNoItemScreen}
        options={{
          tabBarLabel: 'Bag',
          tabBarIcon: ({ color, size }) => {
            return <FontAwesome6Icon name='shopping-bag' size={size} color={color} />
          }
        }}
      />
      <Tab.Screen
        name='Wishlist'
        component={WishlistScreen}
        options={{
          tabBarLabel: 'Wishlist',
          tabBarIcon: ({ color, size }) => {
            return <Ionicons name='heart' size={size + 3} color={color} />
          }
        }}
      />
      <Tab.Screen
        name='Account'
        component={AccountScreen}
        options={{
          tabBarLabel: 'Account',
          tabBarIcon: ({ color, size }) => {
            return <Icon name='cog' size={size} color={color} />
          }
        }}
      />
    </Tab.Navigator>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default BottomRoute
