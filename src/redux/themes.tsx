import { createSlice } from '@reduxjs/toolkit'

const themes = createSlice({
  name: 'themes',
  initialState: false,
  reducers: {
    setStatus: (state) => {
      state = !state
    }
  }
})
export const setReload = themes.actions.setStatus
export default themes.reducer
