import { createSlice } from '@reduxjs/toolkit'
import { Incubator, ModalProps } from 'react-native-ui-lib'
import { DialogProps } from 'react-native-ui-lib/src/incubator'

export const init: DialogProps = {
  visible: false,
  modalProps: { supportedOrientations: ['portrait', 'landscape'] } as ModalProps,
  containerStyle: { bottom: -25, paddingBottom: 25 },
  bottom: true,
  width: '100%',
  ignoreBackgroundPress: true,
  headerProps: { title: 'Title (swipe here)' } as Incubator.DialogHeaderProps
}

const bottomDialog = createSlice({
  name: 'bottomDialog',
  initialState: init,
  reducers: {
    setState: (state, action: { payload: DialogProps }) => {
      Object.assign(state, action.payload)
    }
  }
})
export const setBottomDialog = bottomDialog.actions.setState
export default bottomDialog.reducer
