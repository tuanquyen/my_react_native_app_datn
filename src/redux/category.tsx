import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { getLeafCategories, getRootCategories, getTreeCategories } from '@/services/product.service'
import { Category } from '@/types/app.type'

export const fetchCategories = createAsyncThunk('categories/fetchCategories', async () => {
  const tree = await getTreeCategories()
  const root = await getRootCategories()
  const leaf = await getLeafCategories()
  console.log('OK')
  return { tree, root, leaf }
})

const categories = createSlice({
  name: 'categories',
  initialState: {
    tree: [] as Category[],
    root: [] as Category[],
    leaf: [] as Category[],
    status: false
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchCategories.pending, (state) => {
        state.status = false
      })
      .addCase(fetchCategories.fulfilled, (state, action) => {
        state.status = true
        state.tree = action.payload.tree.data
        state.root = action.payload.root.data
        state.leaf = action.payload.leaf.data
      })
      .addCase(fetchCategories.rejected, (state, action) => {
        state.status = false
      })
  }
})

export default categories.reducer
