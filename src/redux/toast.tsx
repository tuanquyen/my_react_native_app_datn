import { createSlice } from '@reduxjs/toolkit'
import { Incubator } from 'react-native-ui-lib'

const TOAST_ACTIONS = {
  none: {},
  label: { label: 'Undo', onPress: () => console.warn('undo') },
  icon: { onPress: () => console.warn('add') }
}

const toast = createSlice({
  name: 'toast',
  initialState: {
    visible: false,
    position: 'top',
    autoDismiss: 3000,
    swipeable: true
  } as Incubator.ToastProps,
  reducers: {
    setState: (state, action: { payload: Incubator.ToastProps }) => {
      Object.assign(state, action.payload)
    }
  }
})
export const setToast = toast.actions.setState
export default toast.reducer
