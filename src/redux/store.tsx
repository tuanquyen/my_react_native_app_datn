import { configureStore } from '@reduxjs/toolkit'
import themes from './themes'
import categories from './category'
import toast from './toast'
import { DialogProps, Incubator } from 'react-native-ui-lib'
import bottomDialog from './bottomDialog'
import { Category } from '@/types/app.type'
import centerDialog from './centerDialog'
import cart from './cart'
import { GetFromCartData } from '@/services/product.service'

export type SelectionType = {
  toast: Incubator.ToastProps
  bottomDialog: Incubator.DialogProps
  centerDialog: DialogProps
  themes: Boolean
  cart: {
    status: boolean
    variants: GetFromCartData
  }
  categories: {
    status: string
    tree: Category[]
    root: Category[]
    leaf: Category[]
  }
}

export const store = configureStore({
  reducer: {
    cart: cart,
    bottomDialog: bottomDialog,
    centerDialog: centerDialog,
    toast: toast,
    themes: themes,
    categories: categories
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false
    })
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
