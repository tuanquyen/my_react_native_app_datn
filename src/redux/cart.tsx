import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { GetFromCartData, getFromCart } from '@/services/product.service'

export const fetchCart = createAsyncThunk('cart', async () => {
  const { data } = await getFromCart()
  return data
})

const cart = createSlice({
  name: 'cart',
  initialState: {
    variants: [] as GetFromCartData,
    status: false
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchCart.pending, (state) => {
        state.status = false
      })
      .addCase(fetchCart.fulfilled, (state, action) => {
        state.status = true
        state.variants = action.payload
      })
      .addCase(fetchCart.rejected, (state, action) => {
        state.status = false
      })
  }
})

export default cart.reducer
