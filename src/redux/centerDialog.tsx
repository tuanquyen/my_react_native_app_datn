import { createSlice } from '@reduxjs/toolkit'
import { DialogProps, ModalProps } from 'react-native-ui-lib'

export const init: DialogProps = {
  visible: false,
  modalProps: { supportedOrientations: ['portrait', 'landscape'] } as ModalProps,
  containerStyle: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 20,
    marginTop: 200,
    margin: 20,
    alignSelf: 'center'
  },
  top: true,
  ignoreBackgroundPress: true,
  panDirection: 'up'
}

const centerDialog = createSlice({
  name: 'centerDialog',
  initialState: init,
  reducers: {
    setState: (state, action: { payload: DialogProps }) => {
      Object.assign(state, action.payload)
    }
  }
})
export const setCenterDialog = centerDialog.actions.setState
export default centerDialog.reducer
