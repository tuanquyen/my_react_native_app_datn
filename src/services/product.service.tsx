import httpClient from '@/configs/httpRequest'
import { Category, Product, ProductImage, Variant } from '@/types/app.type'
import { StandardResponse } from '@/types/types'
import { getValueStorage } from '@/utils/LocalStorage'

export interface GetTreeCategories extends StandardResponse {
  data: Category[]
}

export interface GetRootCategories extends StandardResponse {
  data: Category[]
}

export interface GetLeafCategories extends StandardResponse {
  data: Category[]
}

export const getTreeCategories = async () => {
  const res = await httpClient.get('/category')
  return res.data as GetTreeCategories
}

export const getRootCategories = async () => {
  const res = await httpClient.get('/category/root')
  return res.data as GetRootCategories
}

export const getLeafCategories = async () => {
  const res = await httpClient.get('/category/leaf')
  return res.data as GetLeafCategories
}

export interface GetNewProducts extends StandardResponse {
  data: {
    id: number
    name: string
    product_image: ProductImage[]
    variants: Variant[]
  }[]
}

export const getNewProducts = async () => {
  const res = await httpClient.get('/product/new?limit=10')
  return res.data as GetNewProducts
}

interface GetProductDetail extends StandardResponse {
  data: Product
}

export const getProductDetail = async (id: number) => {
  const res = await httpClient.get(`/product/detail?product_id=${id}`)
  return res.data as GetProductDetail
}

export type GetFromCartData = (Variant & {
  product: {
    name: string
    product_image: ProductImage[]
  }
} & { quantity: number })[]
export interface GetFromCart extends StandardResponse {
  data: GetFromCartData
}

export const getFromCart = async () => {
  const token = await getValueStorage('token')
  const res = await httpClient.get('/user/cart', { headers: { Authorization: `Bearer ${token}` } })
  return res.data as GetFromCart
}

export const addToCart = async ({ variant_id, quantity }: { variant_id: number; quantity: number }) => {
  const token = await getValueStorage('token')
  const res = await httpClient.post(
    '/user/cart',
    { variant_id, quantity },
    { headers: { Authorization: `Bearer ${token}` } }
  )
  return res.data as StandardResponse
}

export const removeFromCart = async ({ variant_id }: { variant_id: number }) => {
  const token = await getValueStorage('token')
  const res = await httpClient.delete(`/user/cart/?variant_id=${variant_id}`, {
    headers: { Authorization: `Bearer ${token}` }
  })
  return res.data as StandardResponse
}
