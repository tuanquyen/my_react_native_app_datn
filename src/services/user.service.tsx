import httpClient from '@/configs/httpRequest'
import { StandardResponse } from '@/types/types'

interface ICreateUser {
  email: string
  password: string
  passwordConfirmation: string
}

export const signUp = async (data: ICreateUser) => {
  const res = await httpClient.post('/user/create', data)
  return res.data as StandardResponse
}

interface ILogin {
  email: string
  password: string
}

export interface ILoginResponse extends StandardResponse {
  data: {
    token: string
  }
}
export const signIn = async (data: ILogin) => {
  const res = await httpClient.post('/user/login', data)
  return res.data as ILoginResponse
}
