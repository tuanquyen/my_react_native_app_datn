export const FONT_SIZE = {
  xl: 28,
  lg: 22,
  md: 18,
  sm: 14,
  xs: 12
}
export const FONT_LETTER_SPACING = {
  xl: 0.5,
  lg: 0.25,
  md: 0.25,
  sm: 0,
  xs: 0
}
export const FONT_LINE_HEIGHT = {
  xl: 48,
  lg: 36,
  md: 28,
  sm: 24,
  xs: 20
}
export const FONT_ALIGN = {
  left: 'left',
  right: 'right',
  center: 'center'
}

export const FONT_WEIGHT = {
  bold: 'bold',
  normal: 'normal'
}
export const FONT_STYLE = {
  italic: 'italic',
  normal: 'normal'
}
export const FONT_FAMILY = {
  black: 'Inter_900Black',
  extraBold: 'Inter_800ExtraBold',
  bold: 'Inter_700Bold',
  semiBold: 'Inter_600SemiBold',
  medium: 'Inter_500Medium',
  regular: 'Inter_400Regular',
  light: 'Inter_300Light',
  extraLight: 'Inter_200ExtraLight',
  thin: 'Inter_100Thin'
}
