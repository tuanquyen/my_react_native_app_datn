import { FONT_FAMILY, FONT_SIZE } from './declare'
import { DarkTheme as NavigationDarkTheme, DefaultTheme as NavigationDefaultTheme } from '@react-navigation/native'
import { adaptNavigationTheme, MD3LightTheme, MD3DarkTheme } from 'react-native-paper'
import { Typography } from 'react-native-ui-lib'

import merge from 'deepmerge'
const { LightTheme, DarkTheme } = adaptNavigationTheme({
  reactNavigationLight: NavigationDefaultTheme,
  reactNavigationDark: NavigationDarkTheme
})
const CombinedDefaultTheme = merge(MD3LightTheme, LightTheme)
const CombinedDarkTheme = merge(MD3DarkTheme, DarkTheme)

const fonts = {
  bodyLarge: {
    fontFamily: FONT_FAMILY.regular,
    fontSize: FONT_SIZE.lg
  },
  bodyMedium: {
    fontFamily: FONT_FAMILY.regular,
    fontSize: FONT_SIZE.md
  },
  bodySmall: {
    fontFamily: FONT_FAMILY.regular,
    fontSize: FONT_SIZE.sm
  },
  default: {
    fontFamily: FONT_FAMILY.regular,
    fontSize: FONT_SIZE.md
  },
  displayLarge: {
    fontFamily: FONT_FAMILY.semiBold,
    fontSize: FONT_SIZE.lg
  },
  displayMedium: {
    fontFamily: FONT_FAMILY.semiBold,
    fontSize: FONT_SIZE.md
  },
  displaySmall: {
    fontFamily: FONT_FAMILY.semiBold,
    fontSize: FONT_SIZE.sm
  },
  headlineLarge: {
    fontFamily: FONT_FAMILY.bold,
    fontSize: FONT_SIZE.lg
  },
  headlineMedium: {
    fontFamily: FONT_FAMILY.bold,
    fontSize: FONT_SIZE.md
  },
  headlineSmall: {
    fontFamily: FONT_FAMILY.bold,
    fontSize: FONT_SIZE.sm
  },
  labelLarge: {
    fontFamily: FONT_FAMILY.semiBold,
    fontSize: FONT_SIZE.lg
  },
  labelMedium: {
    fontFamily: FONT_FAMILY.semiBold,
    fontSize: FONT_SIZE.sm
  },
  labelSmall: {
    fontFamily: FONT_FAMILY.semiBold,
    fontSize: FONT_SIZE.xs
  },
  titleLarge: {
    fontFamily: FONT_FAMILY.bold,
    fontSize: FONT_SIZE.lg
  },
  titleMedium: {
    fontFamily: FONT_FAMILY.bold,
    fontSize: FONT_SIZE.md
  },
  titleSmall: {
    fontFamily: FONT_FAMILY.bold,
    fontSize: FONT_SIZE.sm
  }
}
export const defaultTheme = {
  ...CombinedDefaultTheme,
  roundness: 10,
  colors: {
    ...CombinedDefaultTheme.colors,
    // The primary key color is used to derive roles for key components across the UI, such as the FAB, prominent buttons, active states, as well as the tint of elevated surfaces.
    primary: '#282828',
    onPrimary: '#ffffff',
    primaryContainer: '#ffffff',
    onPrimaryContainer: '#282828',
    // The secondary key color is used for less prominent components in the UI such as filter chips, while expanding the opportunity for color expression.
    secondary: '#797979',
    // onSecondary: '',
    // secondaryContainer: '',
    // onSecondaryContainer: '',
    // The tertiary key color is used to derive the roles of contrasting accents that can be used to balance primary and secondary colors or bring heightened attention to an element.
    // The tertiary color role is left for teams to use at their discretion and is intended to support broader color expression in products.
    tertiary: '#CECECE',
    // onTertiary: '',
    // tertiaryContainer: '',
    // onTertiaryContainer: '',
    // The neutral key color is used to derive the roles of surface and background, as well as high emphasis text and icons.
    background: '#ffffff',
    onBackground: '#F4F4F4',
    surface: '#6342E8',
    onSurface: '#282828',
    // The neutral variant key color is used to derive medium emphasis text and icons, surface variants, and component outlines.
    surfaceVariant: '#FCAD1E',
    // onSurfaceVariant: '',
    // outline: '',
    // In addition to the accent and neutral key color, the color system includes a semantic color role for error
    error: '#E84242',
    onError: '#ffffff',
    errorContainer: '#ffffff',
    // onErrorContainer: '',
    success: '#006C49',
    onSuccess: '#ffffff',
    successContainer: '#ffffff'
    // onSuccessContainer: '',
  },
  fonts
}
export type AppTheme = typeof defaultTheme

export const darkTheme = {
  ...CombinedDarkTheme,
  colors: {
    ...CombinedDarkTheme.colors,
    // The primary key color is used to derive roles for key components across the UI, such as the FAB, prominent buttons, active states, as well as the tint of elevated surfaces.
    primary: '#282828',
    onPrimary: '#ffffff',
    primaryContainer: '#ffffff',
    // onPrimaryContainer: '',
    // The secondary key color is used for less prominent components in the UI such as filter chips, while expanding the opportunity for color expression.
    secondary: '#797979',
    // onSecondary: '',
    // secondaryContainer: '',
    // onSecondaryContainer: '',
    // The tertiary key color is used to derive the roles of contrasting accents that can be used to balance primary and secondary colors or bring heightened attention to an element.
    // The tertiary color role is left for teams to use at their discretion and is intended to support broader color expression in products.
    tertiary: '#CECECE',
    // onTertiary: '',
    // tertiaryContainer: '',
    // onTertiaryContainer: '',
    // The neutral key color is used to derive the roles of surface and background, as well as high emphasis text and icons.
    background: '#ffffff',
    onBackground: '#F4F4F4',
    surface: '#6342E8',
    onSurface: '#FCAD1E',
    // The neutral variant key color is used to derive medium emphasis text and icons, surface variants, and component outlines.
    surfaceVariant: '#FCAD1E',
    // onSurfaceVariant: '',
    // outline: '',
    // In addition to the accent and neutral key color, the color system includes a semantic color role for error
    error: '#E84242',
    onError: '#ffffff',
    errorContainer: '#ffffff',
    // onErrorContainer: '',
    success: '#006C49',
    onSuccess: '#ffffff',
    successContainer: '#ffffff'
    // onSuccessContainer: '',
  }
}

Typography.loadTypographies(fonts)
