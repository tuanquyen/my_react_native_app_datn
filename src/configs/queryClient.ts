import { MutationCache, QueryCache, QueryClientConfig } from '@tanstack/react-query'

export const queryClientConfig: QueryClientConfig = {
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      refetchOnMount: false,
      refetchOnReconnect: false,
      retry: false,
      staleTime: 5 * 60 * 1000
    }
  },
  queryCache: new QueryCache({
    onError: (error) => {}
  }),
  mutationCache: new MutationCache({
    onError: (error) => {}
  })
}
