import axios from 'axios'

export const FASHION_FUSHION_SERVICE = process.env.EXPO_PUBLIC_API_URL

export async function createPaymentLink(formValue: {
  description: string
  productName: string
  price: number
  returnUrl: string
  cancelUrl: string
}) {
  try {
    const res = await axios({
      method: 'POST',
      url: `${FASHION_FUSHION_SERVICE}/order/create`,
      data: formValue,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    return res.data
  } catch (error: any) {
    return error.response.data
  }
}

export async function getOrder(orderId: string) {
  try {
    const res = await axios({
      method: 'GET',
      url: `${FASHION_FUSHION_SERVICE}/order/${orderId}`,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    return res.data
  } catch (error: any) {
    return error.response.data
  }
}

export async function getBanksList() {
  try {
    const res = await axios({
      method: 'GET',
      url: process.env.REACT_APP_BANKS_LIST_URL,
      headers: {
        'Content-Type': 'application/json'
      }
    })
    return res.data
  } catch (error: any) {
    return error.response.data
  }
}
