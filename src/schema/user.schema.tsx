import { z } from 'zod'
import { emailSchema, nameSchema, phoneNumberSchema } from './common.schema'

const signupPasswordSchema = z
  .string()
  .min(8, {
    message: 'form_error.password_length'
  })
  .refine((password) => /[a-z]/.test(password) && /[A-Z]/.test(password) && /[0-9]/.test(password), {
    message: 'form_error.password_format'
  })

export const loginPasswordSchema = z.string().min(1, { message: 'form_error.provide_password' })

export const signupFormSchema = z
  .object({
    phone_number: phoneNumberSchema,
    email: emailSchema,
    password: signupPasswordSchema,
    passwordConfirmation: z.string().min(8, { message: 'form_error.password_length' })
  })
  .refine((data) => data.password === data.passwordConfirmation, {
    message: 'form_error.password_not_match',
    path: ['passwordConfirmation']
  })
export type ISignUpFormFields = z.infer<typeof signupFormSchema>

export const signinFormSchema = z.object({
  email: emailSchema,
  password: signupPasswordSchema
})

export type ISignInFormFields = z.infer<typeof signinFormSchema>
