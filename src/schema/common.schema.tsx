import { z } from 'zod'

const emailSubaddress = /^[^+]+@[^@]+$/

export const emailSchema = z
  .string()
  .trim()
  .email({ message: 'form_error.email_invalid' })
  .min(1, { message: 'form_error.email_required' })
  .refine(
    (val) => {
      if (process.env.NODE_ENV === 'production') {
        return emailSubaddress.test(val)
      } else {
        return true
      }
    },
    { message: `form_error.email_subaddress` }
  )

export const nameSchema = z
  .string()
  .trim()
  .max(128, { message: 'form_error.name_too_long' })
  .min(1, { message: 'form_error.name_empty' })
  .refine((s) => s.trim().length !== 0, 'form_error.name_space_prefix')

export const phoneNumberSchema = z
  .string()
  .trim()
  .min(4, { message: 'form_error.phone_too_short' })
  .max(11, { message: 'form_error.phone_too_long' })
  .refine((phoneNumber) => /^0[0-9]*$/.test(phoneNumber), {
    message: 'form_error.phone_invalid_format'
  })

export const uriSchema = z.string().trim().url({ message: 'form_error.web_invalid' })
