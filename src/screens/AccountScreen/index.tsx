import { ScrollView } from 'react-native'
import { Text } from 'react-native-ui-lib'
import { SafeAreaView } from 'react-native-safe-area-context'

const AccountScreen = () => {
  return (
    <ScrollView>
      <SafeAreaView>
        <Text>Category Screen</Text>
      </SafeAreaView>
    </ScrollView>
  )
}

export default AccountScreen
