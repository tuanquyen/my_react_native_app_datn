import React, { Component } from 'react'
import { StyleSheet, ScrollView, LayoutAnimation } from 'react-native'
import { Assets, Colors, Typography, View, Drawer, Text, Button, Avatar, Badge, DrawerProps } from 'react-native-ui-lib'
import { gestureHandlerRootHOC } from 'react-native-gesture-handler'
const conversations = [
  {
    name: 'rallylongmailname@wix.com',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail: 'https://i.pravatar.cc/150?img=1',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    thumbnail: 'https://i.pravatar.cc/150?img=2',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail: 'https://i.pravatar.cc/150?img=3',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail: 'https://i.pravatar.cc/150?img=4',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail: 'https://i.pravatar.cc/150?img=5',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail: 'https://i.pravatar.cc/150?img=6',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Sir Robert Walpole',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'A. Schwarzenegger',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Spencer Compton',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Arnold S.',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Henry Pelham',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Arnold Schwarz',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Duke of Newcastle',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Arni Zenegger',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'John Stuart',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Nold Gger',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'George Grenville',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Ard Benegger',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Charles Watson-Wentworth',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'A.B. Schwa',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'William Pitt',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Roni Arnold',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Augustus FitzRoy',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Old Schwarzenegger',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Frederick North',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Bold Schwarzenegger',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Charles Watson-Wentworth',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Mold Schwarzenegger',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'William Petty',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Cold Schwarzenegger',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'William Cavendish-Bentinck',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Hold Schwarzenegger',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Henry Addington',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Bold Schwarz',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'William Grenville',
    text: 'Made a purchase in the total of 7.00$',
    timestamp: '7/14/2016',
    thumbnail:
      'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'S. Zenegger',
    text: 'Get to the chopper',
    timestamp: 'Jul 19th 214'
  },
  {
    name: 'Johnny Gibson',
    text: 'Do you also carry these shoes in black?',
    timestamp: '36 min',
    count: '5',
    // thumbnail: 'https://static.wixstatic.com/media/87994e3d0dda4479a7f4d8c803e1323e.jpg/v1/fit/w_750,h_750/87994e3d0dda4479a7f4d8c803e1323e.jpg',
    isNew: false
  },
  {
    name: 'Jennifer Clark',
    text: 'This might be the subject\nAnd the content is on a new line',
    timestamp: '2 hours',
    count: '1',
    thumbnail:
      'https://static.wixstatic.com/media/c1ca83a468ae4c998fe4fddea60ea84d.jpg/v1/fit/w_750,h_750/c1ca83a468ae4c998fe4fddea60ea84d.jpg',
    isNew: true
  },
  {
    name: 'Rebecka',
    text: 'Yep',
    timestamp: '3 hours',
    count: '12',
    thumbnail:
      'https://static.wixstatic.com/media/43cddb4301684a01a26eaea100162934.jpeg/v1/fit/w_750,h_750/43cddb4301684a01a26eaea100162934.jpeg',
    isNew: true,
    leftTitleBadge: 'badgeOfficial'
  },
  {
    name: 'Murphy',
    text: 'Do you have international shipping?',
    timestamp: '1 Day',
    count: '2',
    thumbnail:
      'https://static.wixstatic.com/media/84e86e9bec8d46dd8296c510629a8d97.jpg/v1/fit/w_750,h_750/84e86e9bec8d46dd8296c510629a8d97.jpg',
    isNew: false
  },
  {
    name: 'Matttt',
    text: 'will get to you next week with that',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/b27921b8c46841b48032f11c16d6e009.jpg/v1/fit/w_750,h_750/b27921b8c46841b48032f11c16d6e009.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  },
  {
    name: 'Brad Taylor',
    text: 'Will I be able to receive it before July 3rd?',
    timestamp: '1 Week',
    count: '99',
    thumbnail:
      'https://static.wixstatic.com/media/7c69c135804b473c9788266540cd90d3.jpg/v1/fit/w_750,h_750/7c69c135804b473c9788266540cd90d3.jpg',
    isNew: false
  },
  {
    name: 'Lina Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/a7adbc41a9f24a64803cac9aec2deb6b.jpg/v1/fit/w_750,h_750/a7adbc41a9f24a64803cac9aec2deb6b.jpg',
    isNew: true,
    leftTitleBadge: 'facebookOn'
  },
  {
    name: 'Marissa Mayer',
    text: 'When will you have them back in stock?',
    timestamp: '1 Week',
    count: '',
    thumbnail: '',
    isNew: true
  },
  {
    name: 'Elliot Brown',
    text: '2 - 3 weeks',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/66003687fdce4e6197cbaf816ca8fd17.jpg/v1/fit/w_750,h_750/66003687fdce4e6197cbaf816ca8fd17.jpg',
    isNew: true
  },
  {
    name: 'Vanessa Campbell',
    text: 'Do you have these in other colors?',
    timestamp: '1 Week',
    count: '',
    thumbnail:
      'https://static.wixstatic.com/media/d4367b20ae2e4036b18c34262d5ed031.jpg/v1/fit/w_750,h_750/d4367b20ae2e4036b18c34262d5ed031.jpg',
    isNew: true,
    leftTitleBadge: 'twitterOn'
  }
]

import { renderBooleanOption, renderSliderOption, renderColorOption } from '../ExampleScreenPresenter'

const ITEMS = {
  read: {
    text: 'Read',
    background: Colors.green30,
    testID: 'left_item_read'
  },
  archive: {
    text: 'Archive',
    background: Colors.blue30,
    testID: 'right_item_archive'
  },
  delete: {
    text: 'Delete',
    background: Colors.red30,
    testID: 'right_item_delete'
  }
}

class DrawerScreen extends Component {
  state = {
    hideItem: false,
    showRightItems: true,
    fullSwipeRight: true,
    showLeftItem: true,
    fullSwipeLeft: true,
    unread: true,
    itemsTintColor: undefined,
    bounciness: undefined,
    itemsIconSize: undefined
  }

  ref: React.Ref<typeof Drawer> = null

  componentDidUpdate(_prevProps: any, prevState: typeof this.state) {
    if (this.state.hideItem && prevState.hideItem) {
      this.showItem()
    }
  }

  deleteItem = () => {
    // TODO: consider including this functionality as part of the drawer component
    setTimeout(() => {
      LayoutAnimation.configureNext({
        update: {
          type: LayoutAnimation.Types.easeInEaseOut,
          property: LayoutAnimation.Properties.scaleY
        },
        delete: {
          type: LayoutAnimation.Types.easeInEaseOut,
          property: LayoutAnimation.Properties.scaleY,
          duration: 2000
        },
        duration: 120
      })
      this.setState({ hideItem: true })
    }, 200)
  }

  toggleReadState = () => {
    this.setState({ unread: !this.state.unread })
  }

  showItem = () => {
    this.setState({ hideItem: false })
  }

  openLeftDrawer = () => {
    if (this.ref) {
      // @ts-expect-error
      this.ref.openLeft()
    }
  }
  openLeftDrawerFull = () => {
    if (this.ref) {
      // @ts-expect-error
      this.ref.openLeftFull()
    }
  }
  toggleLeftDrawer = () => {
    if (this.ref) {
      // @ts-expect-error
      this.ref.toggleLeft()
    }
  }
  openRightDrawer = () => {
    if (this.ref) {
      // @ts-expect-error
      this.ref.openRight()
    }
  }
  openRightDrawerFull = () => {
    if (this.ref) {
      // @ts-expect-error
      this.ref.openRightFull()
    }
  }
  closeDrawer = () => {
    if (this.ref) {
      // @ts-expect-error
      this.ref.closeDrawer()
    }
  }

  renderActions() {
    return (
      <View center marginB-s4>
        <Text text70>Actions</Text>
        <View row>
          <View>
            <Button
              onPress={this.openLeftDrawer}
              label='Open left'
              style={{ margin: 3 }}
              size={'xSmall'}
              testID='open_left_btn'
            />
            <Button
              onPress={this.openLeftDrawerFull}
              label='Full left swipe'
              style={{ margin: 3 }}
              size={'xSmall'}
              testID='swipe_left_btn'
            />
            <Button
              onPress={this.toggleLeftDrawer}
              label='Left toggle'
              style={{ margin: 3 }}
              size={'xSmall'}
              testID='toggle_left_btn'
            />
          </View>

          <View marginH-20>
            <Button onPress={this.closeDrawer} label='Close' style={{ margin: 3 }} size={'xSmall'} testID='close_btn' />
          </View>

          <View>
            <Button
              onPress={this.openRightDrawer}
              label='Open right'
              style={{ margin: 3 }}
              size={'xSmall'}
              testID='open_right_btn'
            />
            <Button
              onPress={this.openRightDrawerFull}
              label='Full right swipe'
              style={{ margin: 3 }}
              size={'xSmall'}
              testID='swipe_right_btn'
            />
          </View>
        </View>
      </View>
    )
  }

  renderListItem() {
    const data = conversations[2]

    return (
      <View
        bg-grey80
        paddingH-20
        paddingV-10
        row
        centerV
        style={{ borderBottomWidth: 1, borderColor: Colors.grey60 }}
        testID='drawer_item'
      >
        {this.state.unread && (
          <Badge
            testID='drawer_item_badge'
            size={6}
            backgroundColor={Colors.red30}
            containerStyle={{ marginRight: 8 }}
          />
        )}
        <Avatar source={{ uri: data.thumbnail }} />
        <View marginL-20>
          <Text text70R={!this.state.unread} text70BO={this.state.unread}>
            {data.name}
          </Text>
          <Text text80 marginT-2>
            {data.text}
          </Text>
        </View>
      </View>
    )
  }

  render() {
    const {
      showRightItems,
      fullSwipeRight,
      showLeftItem,
      fullSwipeLeft,
      itemsTintColor,
      bounciness,
      itemsIconSize,
      hideItem
    } = this.state

    const drawerProps: DrawerProps = {
      itemsTintColor,
      itemsIconSize,
      bounciness,
      // @ts-expect-error
      ref: (component) => (this.ref = component),
      fullSwipeRight,
      onFullSwipeRight: this.deleteItem,
      fullSwipeLeft,
      onWillFullSwipeLeft: this.deleteItem,
      onToggleSwipeLeft: this.toggleReadState,
      testID: 'drawer'
    }
    if (showRightItems) {
      drawerProps.rightItems = [{ ...ITEMS.delete, onPress: this.deleteItem }, ITEMS.archive]
    }

    if (showLeftItem) {
      drawerProps.leftItem = {
        ...ITEMS.read,
        // icon: this.state.unread ? require('../../assets/icons/mail.png') : require('../../assets/icons/refresh.png'),
        text: !this.state.unread ? 'Unread' : 'Read',
        background: this.state.unread ? Colors.green30 : Colors.orange30,
        onPress: this.toggleReadState
      }
    }

    return (
      <View flex>
        <View row padding-20 paddingB-0 marginB-20 centerV>
          <Text text40>Drawer</Text>
          <Button
            link
            grey10
            marginL-s1
            marginT-2
            // iconSource={Assets.icons.demo.refresh}
            onPress={this.showItem}
            disabled={!hideItem}
            testID='refresh_btn'
          />
        </View>
        {!hideItem && (
          <>
            <Drawer key={Date.now()} {...drawerProps}>
              {this.renderListItem()}
            </Drawer>
          </>
        )}

        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View padding-20>
            {this.renderActions()}
            {renderBooleanOption.call(this, 'rightItems', 'showRightItems')}
            {showRightItems && renderBooleanOption.call(this, 'fullSwipeRight', 'fullSwipeRight')}
            {renderBooleanOption.call(this, 'leftItem', 'showLeftItem')}
            {showLeftItem && renderBooleanOption.call(this, 'fullSwipeLeft', 'fullSwipeLeft')}
            {renderColorOption.call(this, 'icon+text color', 'itemsTintColor')}
            {renderSliderOption.call(this, 'bounciness', 'bounciness', {
              min: 5,
              max: 15,
              step: 1,
              initial: 5
            })}
            {renderSliderOption.call(this, 'iconSize', 'itemsIconSize', {
              min: 15,
              max: 25,
              step: 1,
              initial: 20
            })}
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white
  },
  contentContainer: {
    paddingBottom: 50
  },
  drawer: {
    marginTop: 20
  },
  listContent: {
    backgroundColor: Colors.grey80
  },
  rowContent: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.grey80
  },
  rowIcon: {
    width: 38,
    height: 38,
    borderRadius: 19,
    backgroundColor: Colors.violet40,
    margin: 20
  },
  rowTitle: {
    ...Typography.text70,
    fontWeight: 'bold',
    color: Colors.grey20
  },
  rowSubtitle: {
    ...Typography.text80,
    color: Colors.grey30
  },
  rowButtonContainer: {
    flex: 1,
    alignItems: 'flex-end',
    padding: 10
  }
})

export default gestureHandlerRootHOC(DrawerScreen)
