import BackGroundLinear from '@/components/Common/BackgroundLinear'
import Button from '@/components/Common/Button'
import OtherSignIn from '@/components/Common/Form/OtherSignIn'
import SignUpForm from '@/components/Common/Form/SignUpForm'
import Women from '@/static/image/Women'
import { ScrollView } from 'react-native-gesture-handler'
import { useAppTheme } from '@/hooks/useAppTheme'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Text, View } from 'react-native-ui-lib'
import { SignUpProps } from '@/routes/RouteCenter'

const SignUpScreen = ({ navigation, route }: SignUpProps) => {
  const { colors, fonts } = useAppTheme()
  const handleNavigateSignIn = () => {
    navigation.navigate('SignIn')
  }
  return (
    <BackGroundLinear>
      <SafeAreaView>
        <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
          <View gap-20 paddingH-20 paddingT-10>
            <Text style={[fonts.headlineLarge, { color: colors.primary }]}>Đăng ký tài khoản</Text>

            <View absR>
              <Women color={colors.primary} />
            </View>
            <View>
              <SignUpForm />
              <OtherSignIn />
            </View>
            <View row gap-5 center>
              <Text style={[fonts.bodySmall, { color: colors.secondary }]}>Bạn đã có tài khoản?</Text>
              <Button onPress={handleNavigateSignIn}>
                <Text style={[fonts.bodySmall, { color: colors.surface }]}>Đăng nhập</Text>
              </Button>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </BackGroundLinear>
  )
}
export default SignUpScreen
