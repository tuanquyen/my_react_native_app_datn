import { ScrollView } from 'react-native-gesture-handler'
import { useAppTheme } from '@/hooks/useAppTheme'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Text, View } from 'react-native-ui-lib'
import BackGroundLinear from '@/components/Common/BackgroundLinear'
import Women from '@/static/image/Women'
import SignInForm from '@/components/Common/Form/SignInForm'
import OtherSignIn from '@/components/Common/Form/OtherSignIn'
import Button from '@/components/Common/Button'
import { SignInProps } from '@/routes/RouteCenter'

const SignInScreen = ({ navigation, route }: SignInProps) => {
  const { colors, fonts } = useAppTheme()
  const handleNavigateSignUp = () => {
    navigation.navigate('SignUp')
  }
  return (
    <BackGroundLinear>
      <SafeAreaView>
        <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
          <View gap-20 paddingH-20 paddingT-10>
            <Text style={[fonts.headlineLarge, { color: colors.primary }]}>Chào mừng quay trở lại</Text>
            <Text style={[fonts.bodySmall, { width: '70%', lineHeight: 22 }]}>
              Cảm ơn vì đã mua sắm cùng chúng tôi. Chúng tôi đang có nhiều ưu đãi và khuyến mãi hấp dẫn, hãy chọn ngay!
            </Text>
            <View absR>
              <Women color={colors.primary} />
            </View>
            <View>
              <SignInForm email={route.params?.email} password={route.params?.password} />
              <OtherSignIn />
            </View>
            <View row gap-5 center>
              <Text style={[fonts.bodySmall, { color: colors.secondary }]}>Bạn chưa có tài khoản?</Text>
              <Button onPress={handleNavigateSignUp}>
                <Text style={[fonts.bodySmall, { color: colors.surface }]}>Đăng ký ngay</Text>
              </Button>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </BackGroundLinear>
  )
}
export default SignInScreen
