import { ScrollView } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Text, View } from 'react-native-ui-lib'

const WishlistScreen = () => {
  return (
    <View bg-purple40>
      <SafeAreaView>
        <ScrollView>
          <Text style={{ color: 'black' }}>Wishlist Screen</Text>
        </ScrollView>
      </SafeAreaView>
    </View>
  )
}

export default WishlistScreen
