import CategoryCard from '@/components/Category/CategoryCard'
import useGetCategories from '@/hooks/query/useGetCategory'
import { Category } from '@/services/product.service'
import { ScrollView } from 'react-native'
import { useAppTheme } from '@/hooks/useAppTheme'
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context'
import { Text, View } from 'react-native-ui-lib'

const CategoryScreen = () => {
  const { colors, fonts } = useAppTheme()

  const { tree, loading } = useGetCategories()
  return (
    <SafeAreaProvider>
      <SafeAreaView>
        <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
          <View gap-20 padding-10>
            {tree?.map((item: Category, i: number) => <CategoryCard key={i} {...item} />)}
          </View>
        </ScrollView>
      </SafeAreaView>
    </SafeAreaProvider>
  )
}

export default CategoryScreen
