import QuantitySelector from '@/components/QuantitySelector'
import { useAppTheme } from '@/hooks/useAppTheme'
import { Color, Variant } from '@/types/app.type'
import { distinctById, distinctBySize } from '@/utils/function'
import { Text, View } from 'react-native-ui-lib'
import Box from './Box'
import { useState } from 'react'

interface ProductFilteringProps {
  variants: Variant[]
  selectedVariant: Variant | null
  handleSetSelectedVariant: (variant: Variant | null) => void
  selectedQuantity: number
  setSelectedQuantity: (count: number) => void
}

const ProductFiltering = ({
  variants,
  selectedVariant,
  handleSetSelectedVariant,
  selectedQuantity,
  setSelectedQuantity
}: ProductFilteringProps) => {
  const { colors, fonts } = useAppTheme()
  const [variantsFilter, setVariantsFilter] = useState<Variant[]>(variants)
  const [selectedColor, setSelectedColor] = useState<number>(0)
  const [selectedSize, setSelectedSize] = useState<string | null>(null)
  const isShownSize = (item: Variant) => {
    return (
      (!!selectedColor && !selectedSize && variantsFilter.find((x) => x.size === item.size) === undefined) ||
      (!!selectedSize &&
        !!selectedColor &&
        variants.find((x) => x.color_id === selectedColor && x.size === item.size) === undefined)
    )
  }
  const isShownColor = (item: Color) => {
    return (
      (!!selectedSize && !selectedColor && variantsFilter.find((x) => x.color_id === item.id) === undefined) ||
      (!!selectedSize &&
        !!selectedColor &&
        variants.find((x) => x.color_id === item.id && x.size === selectedSize) === undefined)
    )
  }
  const handleSetSelectedColor = (color_id: number) => {
    if (color_id === selectedColor) {
      setSelectedColor(0)
      if (selectedSize) {
        handleSetSelectedVariant(null)
        setVariantsFilter(variants.filter((item) => item.size === selectedSize))
      } else if (!selectedSize) {
        handleSetSelectedVariant(null)
        setVariantsFilter(variants)
      }
    } else {
      setSelectedColor(color_id)
      if (selectedSize) {
        handleSetSelectedVariant(
          variants.find((item) => item.size === selectedSize && item.color_id === color_id) as Variant
        )
      } else if (!selectedSize) {
        setVariantsFilter(variants.filter((item) => item.color_id === color_id))
      }
    }
  }

  const handleSetSelectedSize = (size: string | null) => {
    if (size === selectedSize) {
      setSelectedSize(null)
      if (selectedColor) {
        handleSetSelectedVariant(null)
        setVariantsFilter(variants.filter((item) => item.color_id === selectedColor))
      } else if (!selectedColor) {
        handleSetSelectedVariant(null)
        setVariantsFilter(variants)
      }
    } else {
      setSelectedSize(size)
      if (selectedColor) {
        handleSetSelectedVariant(
          variants.find((item) => item.size === size && item.color_id === selectedColor) as Variant
        )
      } else if (!selectedColor) {
        setVariantsFilter(variants.filter((item) => item.size === size))
      }
    }
  }

  return (
    <>
      <View row spread centerV>
        <Text style={[fonts.headlineSmall, { color: colors.primary }]}>
          Số lượng còn lại: {selectedVariant ? selectedVariant.remain_quantity : ''}
        </Text>
        <QuantitySelector
          max={selectedVariant?.remain_quantity as number}
          min={1}
          count={selectedQuantity}
          setCount={setSelectedQuantity}
          disable={!selectedVariant}
        />
      </View>

      <View gap-5>
        <Text style={[fonts.headlineSmall, { color: colors.primary }]}>Màu sắc</Text>
        <View row gap-10>
          {distinctById(variants.map((item, i) => item.color)).map((item, i) => (
            <Box
              key={i}
              disable={isShownColor(item as Color)}
              selected={item.id === selectedColor}
              color={item.color_code}
              onPress={() => handleSetSelectedColor(item.id)}
            />
          ))}
        </View>
      </View>

      <View gap-5>
        <Text style={[fonts.headlineSmall, { color: colors.primary }]}>Kích thước</Text>
        <View row gap-10>
          {distinctBySize(variants).map((item, i) => (
            <Box
              key={i}
              disable={isShownSize(item)}
              onPress={() => handleSetSelectedSize(item.size)}
              color={item.size === selectedSize ? colors.primary : colors.onPrimary}
              text={item.size}
              textColor={item.size === selectedSize ? colors.onPrimary : colors.primary}
            />
          ))}
        </View>
      </View>
    </>
  )
}

export default ProductFiltering
