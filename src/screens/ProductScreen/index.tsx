import AppBar from '@/components/Common/AppBar'
import { ScrollView } from 'react-native'
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context'
import { AnimatedImage, Carousel, Colors, LoaderScreen, Text, View } from 'react-native-ui-lib'
import BottomBar from './BottomBar'
import { Divider } from 'react-native-paper'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useState } from 'react'
import { formatNumber } from '@/utils/function'
import Button from '@/components/Common/Button'
import BackOrder from '@/static/icon/BackOrder'
import ProductDetail from '@/components/Product/ProductDetail'
import ProductReview from '@/components/Product/ProductReview'
import { useAppTheme } from '@/hooks/useAppTheme'
import { ProductProps } from '@/routes/RouteCenter'
import { useQuery } from '@tanstack/react-query'
import { getProductDetail } from '@/services/product.service'
import { Variant } from '@/types/app.type'
import ProductFiltering from './ProductFiltering'
import NewProduct from '@/components/Home/NewProduct'

const ProductScreen = ({ route, navigation }: ProductProps) => {
  const { colors, fonts } = useAppTheme()
  const { product_id } = route.params
  const [selectedQuantity, setSelectedQuantity] = useState<number>(1)

  const { data, isLoading } = useQuery({
    queryKey: ['product', product_id],
    queryFn: () => getProductDetail(product_id),
    enabled: !!product_id
  })
  const [liked, setLiked] = useState(false)

  const [selectedVariant, setSelectedVariant] = useState<Variant | null>(null)

  const handleSetSelectedVariant = (variant: Variant | null) => {
    setSelectedVariant(variant)
  }

  console.log(selectedVariant)
  if (isLoading) {
    return <LoaderScreen color={Colors.grey40} />
  }

  return (
    <SafeAreaProvider>
      <SafeAreaView style={{ position: 'relative', gap: 5 }}>
        <AppBar
          title='Thông tin sản phẩm'
          onRightPress={() => setLiked(!liked)}
          rightIcon={<AntDesign name={liked ? 'heart' : 'hearto'} color={colors.secondary} size={25} />}
        />

        <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
          <View>
            <Carousel
              autoplay={true}
              alwaysBounceHorizontal={false}
              bounces={false}
              alwaysBounceVertical={false}
              loop={true}
              showCounter={true}
            >
              {data?.data.product_image.map((item, i) => (
                <AnimatedImage
                  key={i}
                  source={{
                    uri: item.image
                  }}
                  style={{ width: '100%', height: 500 }}
                />
              ))}
            </Carousel>
            <View padding-5 gap-10>
              <View row spread centerH gap-5>
                <Text style={[fonts.headlineMedium, { width: '80%', color: colors.primary }]}>{data?.data.name}</Text>
                <View row gap-5 center>
                  <AntDesign name='star' color={colors.surfaceVariant} size={20} />
                  <Text style={[fonts.bodyMedium, { color: colors.primary }]}>{data?.data.avg_rating}</Text>
                </View>
              </View>

              <View row spread gap-5>
                <Price
                  price_export={selectedVariant ? selectedVariant.price_export : data?.data.variants[0].price_export}
                  price_sale={selectedVariant ? selectedVariant.price_sale : data?.data.variants[0].price_sale}
                />
                <View centerV gap-5 right>
                  <Text style={[fonts.bodySmall, { color: colors.primary }]}>({20} đánh giá)</Text>
                  <Text style={[fonts.bodySmall, { color: colors.primary }]}>Đã bán {25}</Text>
                </View>
              </View>
              <Divider />
              <ProductFiltering
                variants={data?.data.variants || []}
                handleSetSelectedVariant={handleSetSelectedVariant}
                selectedVariant={selectedVariant}
                selectedQuantity={selectedQuantity}
                setSelectedQuantity={setSelectedQuantity}
              />

              <Button>
                <View row spread centerV width={'100%'}>
                  <Text style={[fonts.headlineSmall, { color: colors.primary }]}>Xem hướng dẫn kích thước</Text>
                  <AntDesign name='right' />
                </View>
              </Button>

              <Divider />

              <View>
                <Text style={[fonts.headlineSmall, { color: colors.primary }]}>Trả hàng</Text>
                <View row centerV>
                  <BackOrder color={colors.primary} height={40} width={40} />
                  <Text style={[fonts.bodySmall, { color: colors.primary }]}>30 ngày hoàn trả hàng miễn phí</Text>
                </View>
              </View>

              <Divider />

              {data && <ProductDetail {...data.data} />}

              <Divider />

              <ProductReview />
              <NewProduct />
            </View>
            <View height={130}></View>
          </View>
        </ScrollView>

        <BottomBar selectedVaraint={selectedVariant} quantity={selectedQuantity} />
      </SafeAreaView>
    </SafeAreaProvider>
  )
}

export default ProductScreen

const Price = ({
  price_sale,
  price_export
}: {
  price_export: number | null | undefined
  price_sale: number | null | undefined
}) => {
  const { colors, fonts } = useAppTheme()
  if (price_export && price_sale) {
    return (
      <View gap-5>
        <Text style={[fonts.headlineLarge, { color: colors.error }]}>{formatNumber(price_sale)} đ</Text>
        <Text
          style={[
            fonts.displayMedium,
            {
              color: colors.secondary,
              textDecorationStyle: 'solid',
              textDecorationColor: colors.secondary,
              textDecorationLine: 'line-through'
            }
          ]}
        >
          {formatNumber(price_export)} đ
        </Text>
      </View>
    )
  } else if (price_export) {
    return <Text style={[fonts.headlineLarge, { color: colors.error }]}>{formatNumber(price_export)} đ</Text>
  } else if (price_sale) {
    return <Text style={[fonts.headlineLarge, { color: colors.error }]}>{formatNumber(price_sale)} đ</Text>
  } else {
    return <Text style={[fonts.headlineLarge, { color: colors.error }]}>Hàng sắp về</Text>
  }
}
