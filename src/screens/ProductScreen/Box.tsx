import Button from '@/components/Common/Button'
import { useAppTheme } from '@/hooks/useAppTheme'
import { Text, View } from 'react-native-ui-lib'

interface BoxProps {
  selected?: boolean
  color: string
  disable?: boolean
  text?: string
  textColor?: string
  onPress?: () => void
}
const Box = ({ color, disable, text, textColor, onPress, selected }: BoxProps) => {
  const { colors, fonts } = useAppTheme()
  return (
    <Button
      disable={disable}
      onPress={onPress}
      outlineColor={selected ? colors.secondary : colors.background}
      borderRadius={5}
      padding={2}
    >
      <View
        center
        style={[
          {
            width: 30,
            height: 30,
            borderColor: colors.primary,
            borderWidth: 0.5,
            backgroundColor: color,
            borderRadius: 5
          }
        ]}
      >
        <Text style={[fonts.bodyMedium, { color: textColor }]}>{text}</Text>
      </View>
    </Button>
  )
}
export default Box
