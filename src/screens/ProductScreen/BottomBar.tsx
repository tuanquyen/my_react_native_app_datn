import Button from '@/components/Common/Button'
import { useNavigation } from '@react-navigation/native'
import { useAppTheme } from '@/hooks/useAppTheme'
import { Badge, Dialog, PanningProvider, Text, View } from 'react-native-ui-lib'
import FontAwesome6Icon from 'react-native-vector-icons/FontAwesome5'
import BottomBarLayOut from '@/components/Common/BottomBarLayOut'
import { Variant } from '@/types/app.type'
import { useMutation } from '@tanstack/react-query'
import { AxiosError } from '@/types/types'
import { addToCart } from '@/services/product.service'
import { useCallback } from 'react'
import { setToast } from '@/redux/toast'
import { useDispatch } from 'react-redux'
import { MESSAGE } from '@/configs/constant'
import { setCenterDialog } from '@/redux/centerDialog'
import AlertComponent from '@/components/Common/Alert/AlertComponent'
import { fetchCart } from '@/redux/cart'

interface BottomBarProps {
  selectedVaraint: Variant | null
  quantity: number
}
const BottomBar = ({ selectedVaraint, quantity }: BottomBarProps) => {
  const navigation = useNavigation()
  const { colors, fonts } = useAppTheme()
  const dispatch = useDispatch()
  const navigateToCart = () => {
    console.log('OK')
    navigation.navigate('BottomRoute', { screen: 'BagNoItem' })
  }

  const addToCartAction = useMutation({
    mutationFn: addToCart,
    onSuccess: () => {
      dispatch(fetchCart() as any)
      showSuccessToast('Thêm vào giỏ hàng thành công')
    },
    onError: (error: AxiosError<{ status: string }>) => {
      if ((error.response?.data as any)?.error) {
        showFailedToast((error.response?.data as any)?.error.message)
      } else {
        const message = error.response?.data?.message as string
        const closeDialog = () => dispatch(setCenterDialog({ visible: false }))
        if (message === MESSAGE.UNAUTHORIZED) {
          dispatch(
            setCenterDialog({
              visible: true,
              onDismiss: closeDialog,
              children: (
                <AlertComponent
                  title='Phiên đăng nhập đã hết hạn'
                  message='Vui lòng đăng nhập lại'
                  onPressCancel={closeDialog}
                  onPressSuccess={() => {
                    navigation.navigate('SignIn')
                    closeDialog()
                  }}
                />
              )
            })
          )
        } else {
          showFailedToast(message)
        }
      }
    }
  })
  const showSuccessToast = useCallback((message: string) => {
    dispatch(
      setToast({
        visible: true,
        autoDismiss: 3000,
        message,
        messageStyle: { color: colors.success, ...fonts.displaySmall },
        style: {
          paddingVertical: 10,
          backgroundColor: colors.background
        },
        preset: 'success',
        onDismiss: () => dispatch(setToast({ visible: false }))
      })
    )
  }, [])

  const showFailedToast = useCallback((message: string) => {
    dispatch(
      setToast({
        visible: true,
        autoDismiss: 3000,
        message,
        messageStyle: { color: colors.error, ...fonts.displaySmall },
        style: {
          paddingVertical: 10,
          backgroundColor: colors.background
        },
        preset: 'failure',
        onDismiss: () => dispatch(setToast({ visible: false }))
      })
    )
  }, [])
  const handleBuyNow = () => {}
  const handleAddToCart = () => {
    addToCartAction.mutate({ variant_id: selectedVaraint?.id as number, quantity })
  }
  return (
    <BottomBarLayOut>
      <Button width={100} onPress={navigateToCart}>
        <Badge
          backgroundColor={colors.error}
          label='1'
          size={15}
          labelFormatterLimit={2}
          style={{ position: 'absolute', zIndex: 1, top: 0, left: 50 }}
        />
        <FontAwesome6Icon name='shopping-bag' size={25} color={colors.primary} />
      </Button>
      <Button
        disable={!selectedVaraint}
        backgroundColor={colors.primary}
        width={120}
        colorRipple={colors.tertiary}
        padding={10}
        borderRadius={5}
        onPress={handleBuyNow}
      >
        <Text style={[fonts.headlineSmall, { color: colors.onPrimary }]}>Mua ngay</Text>
      </Button>
      <Button
        disable={!selectedVaraint}
        outlineColor={colors.primary}
        width={120}
        colorRipple={colors.tertiary}
        padding={10}
        borderRadius={5}
        onPress={handleAddToCart}
      >
        <Text style={[fonts.headlineSmall, { color: colors.primary }]}>Thêm vào giỏ</Text>
      </Button>
    </BottomBarLayOut>
  )
}

export default BottomBar
