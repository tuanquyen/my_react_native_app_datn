import ActionBar from '@/components/Common/ActionBar'
import Logo from '@/static/image/Logo'
import { Badge, Searchbar } from 'react-native-paper'
import { Button, View } from 'react-native-ui-lib'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { useAppTheme } from '@/hooks/useAppTheme'
import useGetCategories from '@/hooks/query/useGetCategory'

interface AppBarProps {
  searchQuery: string
  setSearchQuery: (query: string) => void
  selectedActionBar: number
  onChageSelectedBar: (index: number) => void
}

const AppBar = ({ searchQuery, setSearchQuery, selectedActionBar, onChageSelectedBar }: AppBarProps) => {
  const { colors, fonts } = useAppTheme()
  const { root } = useGetCategories()
  return (
    <View
      gap-10
      paddingH-10
      style={{
        borderBottomWidth: 0.5,
        borderBottomColor: colors.tertiary,
        paddingBottom: 10
      }}
    >
      <Logo width={300} height={40} />
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
        <Searchbar
          placeholder='Search'
          inputStyle={[
            fonts.bodyMedium,
            { color: colors.secondary, lineHeight: 20, padding: 0, margin: 0, height: '100%' }
          ]}
          style={[{ height: 50, width: '88%', borderRadius: 10, backgroundColor: colors.onBackground }]}
          onChangeText={setSearchQuery}
          value={searchQuery}
        />
        <Button size='xSmall' style={{}} backgroundColor='white'>
          <Badge style={{ position: 'absolute', zIndex: 1, top: 0, right: 18 }}>1</Badge>
          <Ionicons name='notifications-outline' size={30} />
        </Button>
      </View>
      <ActionBar
        selectedIndex={selectedActionBar}
        setSelectedIndex={onChageSelectedBar}
        action={root?.map((item) => ({ label: item.name }))}
      />
    </View>
  )
}

export default AppBar
