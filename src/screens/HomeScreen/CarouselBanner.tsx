import { useAppTheme } from '@/hooks/useAppTheme'
import { Button, Image, Text, View } from 'react-native-ui-lib'

interface CarouselBannerProps {
  title: string
  description: string
  image: string
}

const CarouselBanner = ({ title, description, image }: CarouselBannerProps) => {
  const { colors, fonts } = useAppTheme()
  return (
    <View row br20 paddingH-20 paddingV-10 style={{ backgroundColor: colors.tertiary }}>
      <View flex-3 centerV gap-5>
        <Text style={[fonts.headlineMedium]}>{title}</Text>
        <Text color={colors.secondary} style={fonts.bodySmall}>
          {description}
        </Text>
        <View row>
          <Button
            label='Mua hàng ngay'
            labelProps={{ style: [fonts.labelSmall, { color: colors.onPrimary }] }}
            size='xSmall'
            borderRadius={10}
            paddingV-10
            backgroundColor={colors.primary}
            labelStyle={fonts.labelSmall}
          />
        </View>
      </View>
      <View flex-1>
        <Image source={{ uri: image }} width={100} height={133} borderRadius={5} />
      </View>
    </View>
  )
}

export default CarouselBanner
