import { ScrollView } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Carousel, PageControlPosition, Text, View } from 'react-native-ui-lib'
import GlobalStyle from '@/static/css/GlobalStyle'
import { useState } from 'react'
import CarouselBanner from './CarouselBanner'
import AppBar from './AppBar'
import { useAppTheme } from '@/hooks/useAppTheme'
import NewProduct from '@/components/Home/NewProduct'
import { HomeProps } from '@/routes/BottomRoute'

const HomeScreen = ({ navigation, route }: HomeProps) => {
  const [searchQuery, setSearchQuery] = useState('')
  const [selectedActionBar, setSelectedActionBar] = useState(0)
  const { colors, fonts } = useAppTheme()

  const onChageSelectedBar = (index: number) => {
    setSelectedActionBar(index)
  }
  return (
    <SafeAreaView>
      <AppBar
        onChageSelectedBar={onChageSelectedBar}
        selectedActionBar={selectedActionBar}
        searchQuery={searchQuery}
        setSearchQuery={setSearchQuery}
      />
      <ScrollView style={[GlobalStyle.layout]} showsVerticalScrollIndicator={false}>
        <Carousel
          // onChangePage={() => console.log('page changed')}
          autoplay={true}
          alwaysBounceHorizontal={false}
          bounces={false}
          alwaysBounceVertical={false}
          loop={true}
          showCounter={true}
          pageControlPosition={'under' as PageControlPosition}
        >
          {[
            {
              title: 'Bộ sưu tầm mới',
              description: 'Giảm giá 10% cho lần mua hàng đầu tiên',
              image: 'https://gradshows.uca.ac.uk/wp-content/uploads/2023/05/IMG_4904-2.jpg'
            },
            {
              title: 'Bộ sưu tầm cũ',
              description: 'Giảm giá 10% cho lần mua hàng đầu tiên',
              image: 'https://gradshows.uca.ac.uk/wp-content/uploads/2023/05/IMG_4904-2.jpg'
            }
          ].map((item, i) => (
            <CarouselBanner key={i} {...item} />
          ))}
        </Carousel>
        <NewProduct />
        <View style={{ height: 1000 }}></View>
      </ScrollView>
    </SafeAreaView>
  )
}

export default HomeScreen
