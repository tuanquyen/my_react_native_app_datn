import { StyleSheet, Dimensions } from 'react-native'
import { useAppTheme } from '@/hooks/useAppTheme'
import { SafeAreaView } from 'react-native-safe-area-context'
import { useNavigation } from '@react-navigation/native'
import Onboarding from 'react-native-onboarding-swiper'
import Onboarding1 from '@/static/image/Onboarding1'
import Onboarding2 from '@/static/image/Onboarding2'
import Onboarding3 from '@/static/image/Onboarding3'
import Button from '@/components/Common/Button'
import { Text, View } from 'react-native-ui-lib'
import BackGroundLinear from '@/components/Common/BackgroundLinear'
import { useEffect } from 'react'
import { getValueStorage } from '@/utils/LocalStorage'

const OnboardingScreen = () => {
  const navigation = useNavigation()
  const { colors, fonts } = useAppTheme()
  useEffect(() => {
    const checkSignInBefore = async () => {
      const isSignIn = await getValueStorage('token')
      if (isSignIn) {
        navigation.navigate('BottomRoute', { screen: 'Home' })
      }
    }
    checkSignInBefore()
  }, [])
  const handleSkip = () => {
    navigation.navigate('BottomRoute', { screen: 'Home' })
  }
  const handlNavigateSignIn = () => {
    navigation.navigate('SignIn')
  }
  return (
    <BackGroundLinear>
      <SafeAreaView style={{ flex: 1 }}>
        <Onboarding
          containerStyles={styles.container}
          imageContainerStyles={styles.image}
          flatlistProps={{ scrollEnabled: false } as any}
          pages={[
            {
              backgroundColor: 'transparent',
              image: <Onboarding1 />,
              titleStyles: { ...fonts.headlineLarge, color: colors.primary },
              title: 'Phân loại thời trang thông minh',
              subTitleStyles: { ...fonts.displaySmall, color: colors.secondary, paddingHorizontal: 20 },
              subtitle: 'Thoả thích lựa chọn phong cách của bạn'
            },
            {
              backgroundColor: 'transparent',
              image: <Onboarding2 />,
              title: 'Hỗ trợ thanh toán đa nền tảng',
              titleStyles: { ...fonts.headlineLarge, color: colors.primary },
              subTitleStyles: { ...fonts.displaySmall, color: colors.secondary, paddingHorizontal: 20 },
              subtitle: 'Thanh toán tiền mặt, tài khoản ngân hàng'
            },
            {
              backgroundColor: 'transparent',
              image: <Onboarding3 />,
              title: 'Sáng tạo thời trang theo cách riêng của bạn',
              titleStyles: { ...fonts.headlineLarge, color: colors.primary },
              subTitleStyles: { ...fonts.displaySmall, color: colors.secondary, paddingHorizontal: 20 },
              subtitle: 'FashionFushion giúp bạn tạo ra phong cách riêng của mình'
            }
          ]}
          NextButtonComponent={(props) => (
            <Button {...props} containerStyle={{ paddingRight: 20 }}>
              <Text style={[fonts.headlineSmall, { color: colors.primary }]}>Tiếp theo</Text>
            </Button>
          )}
          SkipButtonComponent={(props) => (
            <Button onPress={handleSkip} containerStyle={{ paddingLeft: 20 }}>
              <Text style={[fonts.displaySmall, { color: colors.secondary }]}>Bỏ qua</Text>
            </Button>
          )}
          showSkip={false}
          showPagination={true}
          bottomBarColor='#fff'
          DoneButtonComponent={() => (
            <View style={{ width: Dimensions.get('window').width, height: 200 }} row gap-50 centerH>
              <Button onPress={handleSkip} width={120} backgroundColor={colors.primary} padding={15} borderRadius={5}>
                <Text style={[fonts.headlineSmall, { color: colors.onPrimary }]}>Mua hàng</Text>
              </Button>
              <Button
                onPress={handlNavigateSignIn}
                width={120}
                outlineColor={colors.primary}
                padding={15}
                borderRadius={5}
              >
                <Text style={[fonts.headlineSmall, { color: colors.primary }]}>Đăng nhập</Text>
              </Button>
            </View>
          )}
        />
      </SafeAreaView>
    </BackGroundLinear>
  )
}

export default OnboardingScreen

const styles = StyleSheet.create({
  container: {
    height: 200,
    justifyContent: 'flex-start',
    paddingTop: 30
  },
  image: {},
  buttonLeft: {
    paddingLeft: 10,
    paddingBottom: 10
  },
  buttonRight: {
    paddingBottom: 10,
    paddingRight: 10
  }
})
