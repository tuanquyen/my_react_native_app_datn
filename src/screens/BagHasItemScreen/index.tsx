import AppBar from '@/components/Common/AppBar'
import BagItem from '@/components/Common/BagItem'
import BottomBarLayOut from '@/components/Common/BottomBarLayOut'
import Button from '@/components/Common/Button'
import useGetCart from '@/hooks/query/useGetCart'
import { useAppTheme } from '@/hooks/useAppTheme'
import { formatNumber } from '@/utils/function'
import { useNavigation } from '@react-navigation/native'
import { useEffect, useState } from 'react'
import { ScrollView } from 'react-native'
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context'
import { Text, View } from 'react-native-ui-lib'

const BagHasItemScreen = () => {
  const navigation = useNavigation()
  const { colors, fonts } = useAppTheme()
  const { variants } = useGetCart()

  const handleBackPress = () => {
    navigation.goBack()
  }

  const [quantities, setQuantities] = useState<number[]>([])
  const [selected, setSelected] = useState<boolean[]>([])
  useEffect(() => {
    if (variants) {
      if (variants.length === 0) {
        navigation.navigate('BottomRoute', { screen: 'BagNoItem' })
      }
      const quantities = variants.map((item) => item.quantity)
      setQuantities(quantities)
      setSelected(variants.map(() => true))
    }
  }, [variants])

  const setQuantity = (index: number, quantity: number) => {
    const newQuantities = [...quantities]
    newQuantities[index] = quantity
    setQuantities(newQuantities)
  }
  const handleSetSelected = (index: number, data: boolean) => {
    const newSelected = [...selected]
    newSelected[index] = data
    setSelected(newSelected)
  }

  const handlePress = () => {}

  return (
    <>
      <SafeAreaProvider>
        <SafeAreaView>
          <AppBar onBackPress={handleBackPress} title='Giỏ hàng' />
          <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
            <View gap-20 padding-10>
              {variants.map((item, index) => (
                <BagItem
                  {...item}
                  selected={selected[index]}
                  setSelected={(selected) => handleSetSelected(index, selected)}
                  setQuantity={(quantity) => setQuantity(index, quantity)}
                  quantity={quantities[index]}
                  image={item.product.product_image[0].image}
                  name={item.product.name}
                  key={index}
                />
              ))}
            </View>
          </ScrollView>
        </SafeAreaView>
        <BottomBarLayOut>
          <View row spread flex-1 centerV>
            <View>
              <Text style={[fonts.headlineMedium, { color: colors.primary }]}>Tổng tiền</Text>
              <Text style={[fonts.headlineLarge, { color: colors.error }]}>
                {formatNumber(
                  variants.reduce(
                    (sum, variant, index) =>
                      sum +
                      ((variant.price_sale ? variant.price_sale : variant.price_export) as number) *
                        quantities[index] *
                        Number(selected[index]),
                    0
                  )
                )}
                {' đ'}
              </Text>
            </View>
            <Button onPress={handlePress} backgroundColor={colors.primary} borderRadius={5} padding={10} width={150}>
              <Text style={[fonts.headlineSmall, { color: colors.onPrimary }]}>Thanh toán</Text>
            </Button>
          </View>
        </BottomBarLayOut>
      </SafeAreaProvider>
    </>
  )
}

export default BagHasItemScreen
