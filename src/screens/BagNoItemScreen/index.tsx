import AppBar from '@/components/Common/AppBar'
import Button from '@/components/Common/Button'
import BagIcon from '@/static/icon/BagIcon'
import { useNavigation } from '@react-navigation/native'
import { ScrollView } from 'react-native'
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context'
import { Text, View } from 'react-native-ui-lib'
import { useAppTheme } from '@/hooks/useAppTheme'

const BagNoItemScreen = () => {
  const navigation = useNavigation()

  const handleNavigateHome = () => {
    navigation.navigate('BottomRoute', { screen: 'Home' })
  }
  const { colors, fonts } = useAppTheme()

  return (
    <SafeAreaProvider>
      <SafeAreaView>
        <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
          <View gap-20 padding-10 centerH>
            <Text style={[fonts.headlineLarge, { color: colors.primary }]}>Giỏ hàng</Text>
            <BagIcon />
            <Text style={[fonts.headlineMedium, { color: colors.primary }]}>Giỏ hàng của bạn đang trống</Text>
            <Text style={[fonts.bodySmall, { textAlign: 'center' }]}>
              Tiếp tục mua sắm để được hưởng những ưu đãi mà chúng tôi dành riêng cho bạn
            </Text>
            <Button onPress={handleNavigateHome} backgroundColor={colors.primary} padding={10} borderRadius={5}>
              <Text style={[fonts.headlineSmall, { color: colors.onPrimary }]}>Tiếp tục mua sắm</Text>
            </Button>
          </View>
        </ScrollView>
      </SafeAreaView>
    </SafeAreaProvider>
  )
}

export default BagNoItemScreen
