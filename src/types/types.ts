import { AxiosRequestConfig, AxiosResponse } from 'axios'

export interface WebHookParamsType {
  cancel: 'true'
  code: '00'
  id: 'efa3d17b09d54065878a2fd5831176fa'
  orderCode: '703084'
  status: 'CANCELLED'
}

export interface TransferInfoType {
  accountName: string
  accountNumber: string
  amount: number
  bin: string
  description: string
  qrCode: string
  orderCode: number
}

export interface AlertType {
  show: boolean
  title?: string
  message?: string
  showConfirm?: boolean
  showCancel?: boolean
  cancelText?: string
  confirmText?: string
  onCancelPressed?: () => void
  onConfirmPressed?: () => void
  confirmButtonColor?: string
  cancelButtonColor?: string
  confirmTextColor?: string
  cancelTextColor?: string
  onDismiss?: () => void
  customView?: React.ReactNode | JSX.Element
  titleStyle?: any
}

export interface StandardResponse {
  code: string
  message: string
}

export interface AxiosError<T = any> extends Error {
  config: AxiosRequestConfig
  code?: string
  request?: any
  response?: AxiosResponse<StandardResponse>
  isAxiosError: boolean
  toJSON: () => object
}
