export interface Category {
  id: number
  name: string
  image: string
  child_categories: Category[]
}

export interface ProductImage {
  id: number
  color_id: number
  image: string
}

export interface Color {
  id: number
  name: string
  color_code: string
}

export interface Variant {
  id: number
  price_sale: number | null
  color_id: number
  price_export: number | null
  size: string
  remain_quantity: number
  product_id: number
  color: Color
}

export interface Product {
  id: number
  name: string
  manufacturer: string
  pattern: string
  style: string
  material: string
  others: {
    name: string
    value: string
  }[]
  created_at: string
  category_id: number
  avg_rating: number
  product_image: ProductImage[]
  variants: Variant[]
}
