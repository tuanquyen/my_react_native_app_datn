import { type RootStackParamList } from '../routes/RouteCenter'

declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamList {}
  }
}
