import * as React from 'react'
import Svg, { Path } from 'react-native-svg'

interface BackOrderProps {
  width?: number
  height?: number
  color: string
}

function BackOrder(props: BackOrderProps) {
  return (
    <Svg width={33} height={34} viewBox='0 0 33 34' fill='none' {...props}>
      <Path d='M20 14.767l3.284.723 1.015-3.205' stroke={props.color} />
      <Path d='M22.454 20.66a7 7 0 11.457-6.05' stroke={props.color} strokeLinecap='round' />
      <Path
        d='M16 21.517v-3.333m0 3.333l-2.08-1.485c-.41-.293-.615-.44-.726-.655-.11-.216-.11-.468-.11-.973V16.1M16 21.517l2.08-1.485c.41-.293.615-.44.726-.655.11-.216.11-.468.11-.973V16.1M16 18.184L13.083 16.1M16 18.184l2.917-2.084m-5.834 0l1.755-1.253c.56-.4.842-.601 1.162-.601.32 0 .601.2 1.163.601l1.754 1.253'
        stroke={props.color}
        strokeLinejoin='round'
      />
    </Svg>
  )
}

export default BackOrder
