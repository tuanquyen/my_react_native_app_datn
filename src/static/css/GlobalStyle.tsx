import { StyleSheet } from 'react-native'

const GlobalStyle = StyleSheet.create({
  layout: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    flexDirection: 'column',
    gap: 10
  }
})

export default GlobalStyle
