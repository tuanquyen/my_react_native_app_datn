import Button from '@/components/Common/Button'
import { useNavigation } from '@react-navigation/native'
import { useAppTheme } from '@/hooks/useAppTheme'
import { Badge, Text, View } from 'react-native-ui-lib'
import FontAwesome6Icon from 'react-native-vector-icons/FontAwesome5'
import { ReactNode } from 'react'
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context'

const BottomBarLayOut = ({ children }: { children?: ReactNode }) => {
  const { colors, fonts } = useAppTheme()

  return (
    <View absB style={{ width: '100%', backgroundColor: colors.background }}>
      <SafeAreaProvider>
        <SafeAreaView>
          <View
            gap-10
            style={{
              zIndex: 1,
              borderColor: colors.tertiary,
              borderWidth: 0.5,
              borderBottomWidth: 0,
              padding: 10,
              marginBottom: 40,
              paddingVertical: 20,
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
              shadowColor: colors.primary,
              shadowOffset: { width: 0, height: -6 },
              shadowOpacity: 0.1,
              shadowRadius: 5,
              backgroundColor: colors.background
            }}
            centerH
            row
          >
            {children}
          </View>
        </SafeAreaView>
      </SafeAreaProvider>
    </View>
  )
}

export default BottomBarLayOut
