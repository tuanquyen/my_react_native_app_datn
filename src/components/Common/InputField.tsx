import { useState } from 'react'
import { useAppTheme } from '@/hooks/useAppTheme'
import { TextField, TextFieldProps } from 'react-native-ui-lib'

const InputField = (props: TextFieldProps) => {
  const [focus, setFocus] = useState(false)
  const { colors, fonts } = useAppTheme()

  return (
    <TextField
      {...props}
      validationMessageStyle={{ ...fonts.bodySmall, color: colors.error, bottom: -25 }}
      containerStyle={{
        marginTop: 25,
        marginBottom: 20,
        backgroundColor: colors.background,
        borderRadius: 10,
        borderColor: props.validationMessage ? colors.error : focus ? colors.surface : colors.tertiary,
        borderWidth: 2,
        shadowColor: colors.tertiary,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 10,
        paddingHorizontal: 15
      }}
      selectionColor={colors.primary}
      onFocus={() => {
        setFocus(true)
      }}
      onBlur={() => {
        setFocus(false)
      }}
      fieldStyle={{ gap: 15 }}
      style={{ color: colors.primary, ...fonts.bodySmall }}
      labelStyle={{ color: colors.primary, top: -30, left: -15, ...fonts.displaySmall }}
    />
  )
}

export default InputField
