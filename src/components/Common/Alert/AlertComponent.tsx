import { Text, View } from 'react-native-ui-lib'
import Button from '../Button'
import { useAppTheme } from '@/hooks/useAppTheme'

interface AlertComponentProps {
  title: string
  message: string
  onPressSuccess?: () => void
  onPressCancel?: () => void
}
export default function AlertComponent({ message, title, onPressCancel, onPressSuccess }: AlertComponentProps) {
  const { colors, fonts } = useAppTheme()
  return (
    <View gap-10 center>
      <Text style={[fonts.headlineMedium, { color: colors.primary }]}>{title}</Text>
      <Text style={[fonts.bodySmall, { color: colors.secondary }]}>{message}</Text>
      <View row right gap-10 style={{ width: '100%' }} centerV paddingV-10>
        <Button onPress={onPressSuccess}>
          <Text style={[fonts.displaySmall, { color: colors.primary }]}>Đồng ý</Text>
        </Button>
        <Button onPress={onPressCancel}>
          <Text style={[fonts.displaySmall, { color: colors.error }]}>Huỷ bỏ</Text>
        </Button>
      </View>
    </View>
  )
}
