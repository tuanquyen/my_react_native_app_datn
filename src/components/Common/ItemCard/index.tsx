import { useAppTheme } from '@/hooks/useAppTheme'
import { AnimatedImage, Text, View } from 'react-native-ui-lib'
import AntDesign from 'react-native-vector-icons/AntDesign'
import BagAdd from '@/static/icon/BagAdd'
import Button from '../Button'
import { formatNumber } from '@/utils/function'
import { useNavigation } from '@react-navigation/native'

interface ItemCardProps {
  id: number
  name: string
  priceSale: number | undefined
  price: number | undefined
  image: string
  isLiked: boolean
}

const ItemCard = ({ id, name, price, priceSale, isLiked, image }: ItemCardProps) => {
  const navigation = useNavigation()
  const { colors, fonts } = useAppTheme()

  const handleOpenProductDetail = () => {
    navigation.navigate('Product', { product_id: id })
  }
  const handlePrice = () => {
    if (price && priceSale) {
      return (
        <>
          <Text style={[fonts.displaySmall, { color: colors.error }]}>{formatNumber(priceSale)} đ</Text>
          <Text
            style={[
              fonts.bodySmall,
              {
                color: colors.secondary,
                fontSize: 12,
                textDecorationStyle: 'solid',
                textDecorationColor: colors.secondary,
                textDecorationLine: 'line-through'
              }
            ]}
          >
            {formatNumber(price)} đ
          </Text>
        </>
      )
    } else if (price && !priceSale) {
      return <Text style={[fonts.displaySmall, { color: colors.error }]}>{formatNumber(price)} đ</Text>
    } else if (!price && priceSale) {
      return <Text style={[fonts.displaySmall, { color: colors.error }]}>{formatNumber(priceSale)} đ</Text>
    } else {
      return <Text style={[fonts.displaySmall, { color: colors.error }]}>Hàng sắp về</Text>
    }
  }
  return (
    <Button onPress={handleOpenProductDetail}>
      <View width={140} height={280} gap-5 style={{ overflow: 'hidden' }}>
        <View
          style={{
            position: 'absolute',
            zIndex: 1,
            right: 0,
            margin: 5,
            width: 30,
            height: 30,
            borderRadius: 30,
            backgroundColor: colors.background,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Button>
            {isLiked && <AntDesign name='heart' color={colors.secondary} size={18} />}
            {!isLiked && <AntDesign name='hearto' color={colors.secondary} size={18} />}
          </Button>
        </View>
        <AnimatedImage
          source={{ uri: image }}
          style={{ width: 140, height: 200 }}
          resizeMode={'cover'}
          borderRadius={10}
        />
        <Text style={[fonts.displaySmall]} numberOfLines={1}>
          {name}
        </Text>
        <View row centerV spread>
          <View gap-2>{handlePrice()}</View>
          <Button width={50} height={30} outlineColor={colors.secondary} borderRadius={5}>
            <BagAdd color={colors.secondary} />
          </Button>
        </View>
      </View>
    </Button>
  )
}

export default ItemCard
