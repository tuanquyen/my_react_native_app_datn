import { Text, View } from 'react-native-ui-lib'
import Button from '../Button'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useAppTheme } from '@/hooks/useAppTheme'
import { ReactNode } from 'react'
import { useNavigation } from '@react-navigation/native'

interface AppBarProps {
  title: string
  onBackPress?: () => void
  onRightPress?: () => void
  rightIcon?: ReactNode
}

const AppBar = ({ title, rightIcon, onBackPress, onRightPress }: AppBarProps) => {
  const { colors, fonts } = useAppTheme()
  const navigation = useNavigation()

  return (
    <View row spread paddingH-10 style={{ alignItems: 'center' }}>
      <Button
        colorRipple={colors.tertiary}
        borderRadius={30}
        padding={10}
        outlineWidth={1}
        outlineColor={colors.tertiary}
        onPress={
          onBackPress
            ? onBackPress
            : () => {
                navigation.goBack()
              }
        }
      >
        <AntDesign name='arrowleft' color={colors.secondary} size={25} />
      </Button>
      <View>
        <Text style={fonts.headlineMedium}>{title}</Text>
      </View>
      <Button
        borderRadius={30}
        disable={rightIcon === undefined}
        colorRipple={colors.tertiary}
        padding={10}
        outlineWidth={1.5}
        outlineColor={rightIcon ? colors.tertiary : colors.background}
        onPress={onRightPress}
      >
        {rightIcon}
      </Button>
    </View>
  )
}

export default AppBar
