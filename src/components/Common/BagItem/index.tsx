import QuantitySelector from '@/components/QuantitySelector'
import { useAppTheme } from '@/hooks/useAppTheme'
import { setBottomDialog } from '@/redux/bottomDialog'
import { useCallback, useState } from 'react'
import { AnimatedImage, Checkbox, Drawer, LoaderScreen, Text, View } from 'react-native-ui-lib'
import { useDispatch } from 'react-redux'
import Button from '../Button'
import { formatNumber } from '@/utils/function'
import { useMutation } from '@tanstack/react-query'
import { AxiosError } from '@/types/types'
import { removeFromCart } from '@/services/product.service'
import { fetchCart } from '@/redux/cart'
import { setToast } from '@/redux/toast'
import { setCenterDialog } from '@/redux/centerDialog'
import { MESSAGE } from '@/configs/constant'
import AlertComponent from '../Alert/AlertComponent'
import { useNavigation } from '@react-navigation/native'
interface BagItemProps {
  id: number
  name: string
  price_export: number | null
  price_sale: number | null
  size: string
  image: string
  quantity: number
  setQuantity: (quantity: number) => void
  remain_quantity: number
  selected: boolean
  setSelected: (selected: boolean) => void
}

const BagItem = ({
  image,
  id,
  size,
  name,
  price_export,
  price_sale,
  quantity,
  setQuantity,
  remain_quantity,
  selected,
  setSelected
}: BagItemProps) => {
  const { colors, fonts } = useAppTheme()
  const dispatch = useDispatch()
  const navigation = useNavigation()
  const setCount = (count: number) => {
    setQuantity(count)
  }

  const showSuccessToast = useCallback((message: string) => {
    dispatch(
      setToast({
        visible: true,
        autoDismiss: 3000,
        message: message,
        messageStyle: { color: colors.success, ...fonts.displaySmall },
        style: {
          paddingVertical: 10,
          backgroundColor: colors.background
        },
        preset: 'success',
        onDismiss: () => dispatch(setToast({ visible: false }))
      })
    )
  }, [])

  const showFailedToast = useCallback((message: string) => {
    dispatch(
      setToast({
        visible: true,
        autoDismiss: 3000,
        message,
        messageStyle: { color: colors.error, ...fonts.displaySmall },
        style: {
          paddingVertical: 10,
          backgroundColor: colors.background
        },
        preset: 'failure',
        onDismiss: () => dispatch(setToast({ visible: false }))
      })
    )
  }, [])

  const { mutate, isPending } = useMutation({
    mutationFn: removeFromCart,
    onSuccess: () => {
      dispatch(fetchCart() as any)
    },
    onError: (error: AxiosError<{ status: string }>) => {
      if ((error.response?.data as any)?.error) {
        showFailedToast((error.response?.data as any)?.error.message)
      } else {
        const message = error.response?.data?.message as string
        const closeDialog = () => dispatch(setCenterDialog({ visible: false }))
        if (message === MESSAGE.UNAUTHORIZED) {
          dispatch(
            setCenterDialog({
              visible: true,
              onDismiss: closeDialog,
              children: (
                <AlertComponent
                  title='Phiên đăng nhập đã hết hạn'
                  message='Vui lòng đăng nhập lại'
                  onPressCancel={closeDialog}
                  onPressSuccess={() => {
                    navigation.navigate('SignIn')
                    closeDialog()
                  }}
                />
              )
            })
          )
        } else {
          showFailedToast(message)
        }
      }
    }
  })

  const handlePressRemove = () => {
    const closeDialog = () => dispatch(setBottomDialog({ visible: false }))
    const handleRemove = () => {
      closeDialog()
      mutate({ variant_id: id })
    }
    dispatch(
      setBottomDialog({
        visible: true,
        onDismiss: closeDialog,
        headerProps: { title: 'Xoá khỏi giỏ hàng', titleStyle: { ...fonts.headlineMedium, color: colors.primary } },
        ignoreBackgroundPress: false,
        children: (
          <View>
            <View row padding-5 marginH-20 gap-10 centerV backgroundColor={colors.background} style={{ zIndex: 100 }}>
              <AnimatedImage
                source={{
                  uri: image
                }}
                style={{ width: 90, height: 90, borderRadius: 5 }}
                flex-3
              />
              <View gap-5 flex-7>
                <Text style={[fonts.displaySmall, { color: colors.primary }]} numberOfLines={2}>
                  {name}
                </Text>
                <View row spread centerV>
                  <ProductSizePrice price_export={price_export} price_sale={price_sale} size={size} />
                  <Text>Số lượng: {quantity}</Text>
                </View>
              </View>
            </View>
            <View row spread gap-20 margin-20>
              <Button
                onPress={closeDialog}
                containerStyle={{ flex: 1 }}
                outlineColor={colors.primary}
                padding={10}
                borderRadius={5}
              >
                <Text style={[fonts.headlineSmall, { color: colors.primary }]}>Huỷ bỏ</Text>
              </Button>
              <Button
                onPress={handleRemove}
                containerStyle={{ flex: 1 }}
                backgroundColor={colors.primary}
                padding={10}
                borderRadius={5}
              >
                <Text style={[fonts.headlineSmall, { color: colors.onPrimary }]}>Xác nhận</Text>
              </Button>
            </View>
          </View>
        )
      })
    )
  }
  return (
    <Drawer
      rightItems={[
        {
          background: colors.error,
          onPress: handlePressRemove,
          icon: require('../../../static/icon/RemoveIcon.png'),
          style: { gap: 10, height: 100 }
        }
      ]}
      style={{ borderColor: colors.tertiary, borderWidth: 1, borderRadius: 5 }}
    >
      {isPending && <LoaderScreen />}
      <View row padding-5 gap-10 centerV backgroundColor={colors.background} style={{ zIndex: 100 }}>
        <Checkbox value={selected} onValueChange={() => setSelected(!selected)} color={colors.primary} flex-1 />
        <AnimatedImage
          source={{
            uri: image
          }}
          style={{ width: 90, height: 90, borderRadius: 5 }}
          flex-3
        />
        <View gap-5 flex-11>
          <Text style={[fonts.displaySmall, { color: colors.primary }]} flex-1 numberOfLines={2}>
            {name}
          </Text>
          <View row spread centerV>
            <ProductSizePrice price_export={price_export} price_sale={price_sale} size={size} />
            <QuantitySelector count={quantity} setCount={setCount} max={remain_quantity} min={1} />
          </View>
        </View>
      </View>
    </Drawer>
  )
}
export default BagItem

const ProductSizePrice = ({
  price_export,
  price_sale,
  size
}: {
  price_export: number | null
  price_sale: number | null
  size: string
}) => {
  const { colors, fonts } = useAppTheme()
  if (price_export && price_sale) {
    return (
      <View gap-2 left>
        <Text style={[fonts.bodySmall, { color: colors.secondary }]}>Size: {size}</Text>
        <Text style={[fonts.displaySmall, { color: colors.error }]}>{formatNumber(price_sale)} đ</Text>
        <Text
          style={[
            fonts.bodySmall,
            {
              color: colors.secondary,
              textDecorationStyle: 'solid',
              textDecorationColor: colors.secondary,
              textDecorationLine: 'line-through'
            }
          ]}
        >
          {formatNumber(price_export)} đ
        </Text>
      </View>
    )
  } else if (!price_sale && price_export) {
    return (
      <View gap-2 left>
        <Text style={[fonts.bodySmall, { color: colors.secondary }]}>Size: {size}</Text>
        <Text style={[fonts.displaySmall, { color: colors.error }]}>{formatNumber(price_export)} đ</Text>
      </View>
    )
  }
}
