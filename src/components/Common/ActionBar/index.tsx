import { Button, View } from 'react-native-ui-lib'
import { ScrollView } from 'react-native'
import { useAppTheme } from '@/hooks/useAppTheme'

interface ActionBarProps {
  action: ButtonProps[]
  selectedIndex: number
  setSelectedIndex: (index: number) => void
}

interface ButtonProps {
  label: string
}
const ActionBar = ({ selectedIndex, action, setSelectedIndex }: ActionBarProps) => {
  const { colors } = useAppTheme()

  return (
    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} alwaysBounceHorizontal={false}>
      <View row spread gap-10>
        {action.map((button, index) => {
          return index === selectedIndex ? (
            <Button
              key={index}
              label={button.label}
              backgroundColor={colors.primary}
              labelStyle={{ color: colors.background }}
              borderRadius={10}
              onPress={() => setSelectedIndex(index)}
            />
          ) : (
            <Button
              key={index}
              label={button.label}
              backgroundColor={colors.onBackground}
              labelStyle={{ color: colors.primary }}
              borderRadius={10}
              onPress={() => setSelectedIndex(index)}
            />
          )
        })}
      </View>
    </ScrollView>
  )
}
export default ActionBar
