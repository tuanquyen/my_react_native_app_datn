import { useAppTheme } from '@/hooks/useAppTheme'
import { Text, View } from 'react-native-ui-lib'
import Fontisto from 'react-native-vector-icons/Fontisto'
import Feather from 'react-native-vector-icons/Feather'
import InputField from '../InputField'
import { useCallback, useEffect, useState } from 'react'
import Button from '../Button'
import { ISignInFormFields, signinFormSchema } from '@/schema/user.schema'
import { zodResolver } from '@hookform/resolvers/zod'
import { Controller, useForm } from 'react-hook-form'
import { useMutation } from '@tanstack/react-query'
import { ILoginResponse, signIn } from '@/services/user.service'
import { useDispatch } from 'react-redux'
import { setToast } from '@/redux/toast'
import { useNavigation } from '@react-navigation/native'
import { AxiosError } from '@/types/types'
import { setValueStorage } from '@/utils/LocalStorage'
import { set } from 'lodash'
interface SignInFormProps {
  email: string | undefined
  password: string | undefined
}
const SignInForm = ({ email, password }: SignInFormProps) => {
  const navigation = useNavigation()
  const [showPassword, setShowPassword] = useState(false)
  const dispatch = useDispatch()
  const handleSecureTextEntry = () => {
    setShowPassword(!showPassword)
  }

  function showEyePassword() {
    if (showPassword) return <Feather name='eye' onPress={handleSecureTextEntry} size={20} color={colors.secondary} />
    return <Feather name='eye-off' size={20} onPress={handleSecureTextEntry} color={colors.secondary} />
  }
  const { colors, fonts } = useAppTheme()

  const {
    handleSubmit,
    control,
    setValue,
    reset,
    formState: { errors, isValid }
  } = useForm<ISignInFormFields>({
    resolver: zodResolver(signinFormSchema),
    mode: 'onChange'
  })

  useEffect(() => {
    if (email && password) {
      setValue('email', email, { shouldValidate: true })
      setValue('password', password, { shouldValidate: true })
    }
  }, [email, password])

  const showSuccessToast = useCallback(() => {
    dispatch(
      setToast({
        visible: true,
        autoDismiss: 3000,
        message: 'Đăng nhập thành công',
        messageStyle: { color: colors.success, ...fonts.displaySmall },
        style: {
          paddingVertical: 10,
          backgroundColor: colors.background
        },
        preset: 'success',
        onDismiss: () => dispatch(setToast({ visible: false }))
      })
    )
  }, [])

  const showFailedToast = useCallback((message: string) => {
    dispatch(
      setToast({
        visible: true,
        autoDismiss: 3000,
        message,
        messageStyle: { color: colors.error, ...fonts.displaySmall },
        style: {
          paddingVertical: 10,
          backgroundColor: colors.background
        },
        preset: 'failure',
        onDismiss: () => dispatch(setToast({ visible: false }))
      })
    )
  }, [])

  const { isPending, mutate } = useMutation({
    mutationFn: signIn,
    onSuccess: ({ data }: ILoginResponse) => {
      showSuccessToast()
      reset()
      setValueStorage('token', data.token)
      navigation.navigate('BottomRoute', { screen: 'Home' })
    },
    onError: (error: AxiosError<{ status: string }>) => {
      showFailedToast(error.response?.data?.message as string)
    }
  })
  const onSubmit = (data: ISignInFormFields) => {
    mutate(data)
  }
  return (
    <View marginV-40 gap-10>
      <Controller
        name='email'
        control={control}
        render={({ field }) => (
          <InputField
            placeholder={'Nhập email của bạn'}
            leadingAccessory={<Fontisto name='email' size={20} color={colors.secondary} />}
            label='Email'
            onChangeText={field.onChange}
            enableErrors
            validationMessage={errors.email?.message as string}
            maxLength={30}
            value={field.value}
          />
        )}
      />
      <Controller
        name='password'
        control={control}
        render={({ field }) => {
          return (
            <InputField
              placeholder={'Nhập mật khẩu'}
              leadingAccessory={<Feather name='lock' size={20} color={colors.secondary} />}
              trailingAccessory={showEyePassword()}
              label='Mật khẩu'
              secureTextEntry={!showPassword}
              onChangeText={field.onChange}
              value={field.value}
              enableErrors
              validationMessage={errors.password?.message as string}
              maxLength={30}
            />
          )
        }}
      />
      <View row right>
        <Button>
          <Text style={[fonts.bodySmall, { color: colors.surface }]}>Quên mật khẩu?</Text>
        </Button>
      </View>
      <Button
        onPress={handleSubmit(onSubmit)}
        loading={isPending}
        disable={isPending || !isValid}
        colorRipple={colors.secondary}
        backgroundColor={colors.primary}
        padding={10}
        borderRadius={10}
        containerStyle={{ marginTop: 20 }}
      >
        <Text style={[fonts.headlineMedium, { color: colors.onPrimary }]}>Đăng nhập</Text>
      </Button>
    </View>
  )
}

export default SignInForm
