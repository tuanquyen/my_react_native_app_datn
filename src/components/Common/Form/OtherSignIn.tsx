import { useAppTheme } from '@/hooks/useAppTheme'
import { Text, View } from 'react-native-ui-lib'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Button from '../Button'
import { useNavigation } from '@react-navigation/native'

const OtherSignIn = () => {
  const navigation = useNavigation()
  const { colors, fonts } = useAppTheme()
  const onFacebookSignIn = () => {}
  return (
    <View gap-5>
      <View row gap-5 center>
        <AntDesign name='minus' color={colors.primary} size={20} />
        <Text style={[fonts.bodySmall, { color: colors.secondary }]}>Hoặc đăng nhập bằng</Text>
        <AntDesign name='minus' color={colors.primary} size={20} />
      </View>
      <View row gap-5 center>
        <Button outlineColor={colors.secondary} padding={5} borderRadius={5}>
          <AntDesign name='google' color={colors.primary} size={20} />
          <Text>Google</Text>
        </Button>
        <Button outlineColor={colors.secondary} padding={5} borderRadius={5}>
          <AntDesign name='apple1' color={colors.primary} size={20} />
          <Text>Apple</Text>
        </Button>
        <Button onPress={onFacebookSignIn} outlineColor={colors.secondary} padding={5} borderRadius={5}>
          <AntDesign name='facebook-square' color={colors.primary} size={20} />
          <Text>Facebook</Text>
        </Button>
      </View>
    </View>
  )
}
export default OtherSignIn
