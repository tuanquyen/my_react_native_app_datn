import { useAppTheme } from '@/hooks/useAppTheme'
import { Text, View } from 'react-native-ui-lib'
import Fontisto from 'react-native-vector-icons/Fontisto'
import Feather from 'react-native-vector-icons/Feather'
import InputField from '../InputField'
import { useCallback, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import Button from '../Button'
import { ISignUpFormFields, signupFormSchema } from '@/schema/user.schema'
import { zodResolver } from '@hookform/resolvers/zod'
import { useMutation } from '@tanstack/react-query'
import { signUp } from '@/services/user.service'
import { useDispatch } from 'react-redux'
import { setToast } from '@/redux/toast'
import { AxiosError } from '@/types/types'
import { useNavigation } from '@react-navigation/native'

const SignUpForm = () => {
  const navigation = useNavigation()
  const { colors, fonts } = useAppTheme()
  const [showPassword, setShowPassword] = useState(false)
  const [showConfirmPassword, setShowConfirmPassword] = useState(false)
  const dispatch = useDispatch()
  const handleShowPassword = () => {
    setShowPassword(!showPassword)
  }
  const handleShowConfirmPassword = () => {
    setShowConfirmPassword(!showConfirmPassword)
  }

  function showEyePassword() {
    if (showPassword) return <Feather name='eye' onPress={handleShowPassword} size={20} color={colors.secondary} />
    return <Feather name='eye-off' size={20} onPress={handleShowPassword} color={colors.secondary} />
  }
  function showEyeConfirmPassword() {
    if (showConfirmPassword)
      return <Feather name='eye' onPress={handleShowConfirmPassword} size={20} color={colors.secondary} />
    return <Feather name='eye-off' size={20} onPress={handleShowConfirmPassword} color={colors.secondary} />
  }

  const showSuccessToast = useCallback(() => {
    dispatch(
      setToast({
        visible: true,
        autoDismiss: 3000,
        message: 'Đăng ký thành công',
        messageStyle: { color: colors.success, ...fonts.displaySmall },
        style: {
          paddingVertical: 10,
          backgroundColor: colors.background
        },
        preset: 'success',
        onDismiss: () => dispatch(setToast({ visible: false }))
      })
    )
  }, [])

  const showFailedToast = useCallback((message: string) => {
    dispatch(
      setToast({
        visible: true,
        autoDismiss: 3000,
        message,
        messageStyle: { color: colors.error, ...fonts.displaySmall },
        style: {
          paddingVertical: 10,
          backgroundColor: colors.background
        },
        preset: 'failure',
        onDismiss: () => dispatch(setToast({ visible: false }))
      })
    )
  }, [])

  const { isPending, mutate } = useMutation({
    mutationFn: signUp,
    onSuccess: async () => {
      showSuccessToast()
      const email = getValues('email')
      const password = getValues('password')
      // reset()
      navigation.navigate('SignIn', { email: email, password: password })
    },
    onError: (error: AxiosError<{ status: string }>) => {
      if ((error.response?.data as any)?.error) {
        showFailedToast((error.response?.data as any)?.error.message)
      } else {
        showFailedToast(error.response?.data?.message as string)
      }
    }
  })

  const {
    handleSubmit,
    getValues,
    control,
    reset,
    formState: { errors, isDirty, isValid }
  } = useForm<ISignUpFormFields>({
    resolver: zodResolver(signupFormSchema),
    mode: 'onChange'
  })

  const onSubmit = (data: ISignUpFormFields) => {
    mutate(data)
  }

  return (
    <View marginV-20 gap-10>
      <Controller
        name='email'
        control={control}
        defaultValue=''
        render={({ field }) => (
          <InputField
            placeholder={'Nhập email của bạn'}
            leadingAccessory={<Fontisto name='email' size={20} color={colors.secondary} />}
            label='Email'
            onChangeText={field.onChange}
            enableErrors
            validationMessage={errors.email?.message as string}
            maxLength={30}
            value={field.value}
          />
        )}
      />
      <Controller
        name='phone_number'
        control={control}
        defaultValue=''
        render={({ field }) => (
          <InputField
            placeholder={'Nhập số điện thoại của bạn'}
            leadingAccessory={<Feather name='phone' size={20} color={colors.secondary} />}
            label='Số điện thoại'
            onChangeText={field.onChange}
            enableErrors
            validationMessage={errors.phone_number?.message as string}
            maxLength={30}
            value={field.value}
          />
        )}
      />
      <Controller
        name='password'
        control={control}
        defaultValue=''
        render={({ field }) => (
          <InputField
            placeholder={'Nhập mật khẩu'}
            leadingAccessory={<Feather name='lock' size={20} color={colors.secondary} />}
            trailingAccessory={showEyePassword()}
            label='Mật khẩu'
            secureTextEntry={!showPassword}
            onChangeText={field.onChange}
            value={field.value}
            enableErrors
            validationMessage={errors.password?.message as string}
            maxLength={30}
          />
        )}
      />
      <Controller
        name='passwordConfirmation'
        control={control}
        defaultValue=''
        render={({ field }) => (
          <InputField
            placeholder={'Nhập lại mật khẩu'}
            leadingAccessory={<Feather name='lock' size={20} color={colors.secondary} />}
            trailingAccessory={showEyeConfirmPassword()}
            label='Nhập lại mật khẩu'
            secureTextEntry={!showConfirmPassword}
            onChangeText={field.onChange}
            value={field.value}
            enableErrors
            validationMessage={errors.passwordConfirmation?.message as string}
            maxLength={30}
          />
        )}
      />

      <Button
        colorRipple={colors.secondary}
        backgroundColor={colors.primary}
        loading={isPending}
        disable={isPending || !isDirty || !isValid}
        padding={10}
        borderRadius={10}
        containerStyle={{ marginTop: 20 }}
        onPress={handleSubmit(onSubmit)}
      >
        <Text style={[fonts.headlineMedium, { color: colors.onPrimary }]}>Đăng ký</Text>
      </Button>
    </View>
  )
}

export default SignUpForm
