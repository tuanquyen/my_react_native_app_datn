import { ReactNode } from 'react'
import { AnimatableNumericValue, DimensionValue, Pressable } from 'react-native'
import { Text, View } from 'react-native-ui-lib'
import { ViewStyle } from 'react-native'
import { ActivityIndicator } from 'react-native-paper'

interface ButtonProps {
  backgroundColor?: string
  colorRipple?: string
  outlineColor?: string
  outlineWidth?: number
  width?: DimensionValue
  height?: number
  borderRadius?: number
  children?: ReactNode
  padding?: number
  margin?: number
  onPress?: () => void
  style?: ViewStyle
  containerStyle?: ViewStyle
  disable?: Boolean
  opacity?: AnimatableNumericValue
  loading?: Boolean
}

const Button = ({
  padding,
  backgroundColor,
  outlineColor,
  outlineWidth,
  colorRipple,
  width,
  height,
  borderRadius,
  opacity,
  children,
  margin,
  onPress,
  style,
  containerStyle,
  disable,
  loading
}: ButtonProps) => {
  const bg = backgroundColor ? backgroundColor : 'transparent'
  const opacityValue = opacity ? opacity : 0.5
  return (
    <View style={[{ borderRadius, overflow: 'hidden', width, ...containerStyle, opacity: disable ? 0.5 : 1 }]}>
      <Pressable
        disabled={disable ? true : false}
        android_ripple={{ color: colorRipple ? colorRipple : 'transparent' }}
        onPress={onPress}
        style={({ pressed }) => ({
          backgroundColor: colorRipple ? (pressed ? colorRipple : bg) : bg,
          overflow: 'hidden',
          borderColor: outlineColor ? outlineColor : 'transparent',
          borderWidth: outlineWidth ? outlineWidth : 1,
          justifyContent: 'center',
          alignItems: 'center',
          opacity: pressed ? opacityValue : 1,
          flexDirection: 'row',
          gap: 2,
          width,
          height,
          borderRadius,
          padding,
          margin,
          ...style
        })}
      >
        {!loading ? children : <ActivityIndicator size='small' color='white' animating={true} />}
      </Pressable>
    </View>
  )
}

export default Button
