import { ReactNode } from 'react'
import { useAppTheme } from '@/hooks/useAppTheme'
import { LinearGradient } from 'expo-linear-gradient'
interface BackGroundLinearProps {
  children: ReactNode
}
const BackGroundLinear = ({ children }: BackGroundLinearProps) => {
  const { colors } = useAppTheme()
  return (
    <LinearGradient
      colors={[colors.primary, '#FFF']}
      style={{ flex: 1 }}
      start={{ x: 0, y: -0.5 }}
      end={{ x: 0, y: 0.5 }}
    >
      {children}
    </LinearGradient>
  )
}

export default BackGroundLinear
