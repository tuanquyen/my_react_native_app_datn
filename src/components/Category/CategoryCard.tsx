import { useState } from 'react'
import { AnimatedImage, Text, View } from 'react-native-ui-lib'
import Button from '../Common/Button'
import { LayoutAnimation, Platform, UIManager } from 'react-native'
import { Category } from '@/services/product.service'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { useAppTheme } from '@/hooks/useAppTheme'

if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
  UIManager.setLayoutAnimationEnabledExperimental(true)
}
interface CategoryCardProps extends Category {}

const CategoryCard = ({ name, image, child_categories }: CategoryCardProps) => {
  const { colors, fonts } = useAppTheme()
  const [expanded, setExpanded] = useState(false)

  const handleExpand = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)

    setExpanded(!expanded)
  }
  return (
    <View>
      <Button
        width={'100%'}
        opacity={1}
        onPress={handleExpand}
        backgroundColor={colors.tertiary}
        containerStyle={{
          borderRadius: 10,
          borderBottomLeftRadius: expanded ? 0 : 10,
          borderBottomRightRadius: expanded ? 0 : 10
        }}
      >
        <View row spread style={{ width: '100%', height: 100 }}>
          <View row gap-10 centerV padding-10>
            <Text style={[fonts.headlineLarge, { color: colors.primary }]}>{name}</Text>
            {expanded ? (
              <AntDesign name='up' color={colors.primary} size={20} />
            ) : (
              <AntDesign name='down' color={colors.primary} size={20} />
            )}
          </View>
          <AnimatedImage source={{ uri: image }} style={{ width: 100, height: 100 }} />
        </View>
      </Button>
      {expanded && (
        <View
          style={{ backgroundColor: colors.tertiary, padding: 10, paddingHorizontal: 20, opacity: 0.8, height: 100 }}
        >
          {child_categories.map((item, i) => (
            <Button key={i}>
              <View style={{ width: '100%' }} row spread>
                <Text style={[fonts.bodyMedium, { color: colors.primary }]}>{item.name}</Text>
                {item.child_categories.length > 0 && <AntDesign name='right' size={15} color={colors.primary} />}
              </View>
            </Button>
          ))}
        </View>
      )}
    </View>
  )
}

export default CategoryCard
