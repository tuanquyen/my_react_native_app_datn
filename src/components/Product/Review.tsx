import { useState } from 'react'
import { useAppTheme } from '@/hooks/useAppTheme'
import { AnimatedImage, Avatar, Image, Modal, Text, View } from 'react-native-ui-lib'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Octicons from 'react-native-vector-icons/Octicons'

import Button from '../Common/Button'
import dayjs from 'dayjs'
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context'
import AppBar from '../Common/AppBar'
import { ScrollView } from 'react-native'

interface ReviewProps {
  name: string
  avatar: string
  review: {
    rating: number
    content: string
    image: string
    createdAt: string
  }
}

const Review = ({ review, name, avatar }: ReviewProps) => {
  const { colors, fonts } = useAppTheme()
  const [isVisible, setIsVisible] = useState(false)
  const [showFullText, setShowFullText] = useState(false)

  const handleShowModal = () => {
    setIsVisible(!isVisible)
  }
  const handleShowFullText = () => {
    setShowFullText(!showFullText)
  }
  return (
    <View row gap-10 marginH-20>
      <Avatar
        source={{
          uri: 'https://lh3.googleusercontent.com/-cw77lUnOvmI/AAAAAAAAAAI/AAAAAAAAAAA/WMNck32dKbc/s181-c/104220521160525129167.jpg'
        }}
        label='TVA'
        name='Trần Văn A'
        size={30}
      />
      <View gap-5>
        <View row spread>
          <View>
            <Text style={[fonts.labelSmall, { color: colors.primary }]}>Lê Quốc Trạng</Text>
            <View row>
              {[...Array(4)].map((_, i) => (
                <AntDesign name='star' color={colors.surfaceVariant} size={12} key={i} />
              ))}
              {[...Array(5 - 4)].map((_, i) => (
                <AntDesign name='staro' color={colors.surfaceVariant} size={12} key={i} />
              ))}
            </View>
          </View>
          <Button>
            <Octicons name='report' color={colors.secondary} size={15} />
          </Button>
        </View>

        <View style={{ width: '90%' }}>
          <Text
            style={[fonts.bodySmall, { fontSize: 12, color: colors.primary }]}
            numberOfLines={showFullText ? 20 : 2}
          >
            {review.content}
          </Text>
          {review.content.length > 100 && (
            <View
              style={[
                { position: 'absolute', backgroundColor: colors.background, paddingHorizontal: 5, bottom: 0, right: 0 }
              ]}
            >
              <Button onPress={handleShowFullText}>
                <Text style={[fonts.bodySmall, { fontSize: 12, color: colors.secondary }]}>
                  {showFullText ? 'Thu lại' : 'Xem thêm'}
                </Text>
              </Button>
            </View>
          )}
        </View>
        <View>
          <Button style={{ justifyContent: 'flex-start' }} onPress={handleShowModal}>
            <Image
              borderRadius={10}
              source={{
                uri: review.image
              }}
              style={{ width: '50%', height: 120 }}
            />
          </Button>
        </View>

        <Text style={[fonts.bodySmall, { fontSize: 12, color: colors.secondary }]}>
          {dayjs(review.createdAt).format('hh:mm A MM/DD/YYYY')}
        </Text>
      </View>
      <Modal visible={isVisible} onBackgroundPress={handleShowModal}>
        <SafeAreaProvider>
          <SafeAreaView style={{ flex: 1, backgroundColor: 'transparent', gap: 5 }}>
            <AppBar title='Ảnh đánh giá' onBackPress={handleShowModal} />
            <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
              <Image
                borderRadius={10}
                source={{
                  uri: review.image
                }}
                style={{ aspectRatio: 0.8, width: '100%' }}
              />
              <View padding-10 row gap-5>
                <Avatar
                  source={{
                    uri: 'https://lh3.googleusercontent.com/-cw77lUnOvmI/AAAAAAAAAAI/AAAAAAAAAAA/WMNck32dKbc/s181-c/104220521160525129167.jpg'
                  }}
                  label='TVA'
                  name='Trần Văn A'
                  size={40}
                />
                <View gap-5>
                  <View row spread>
                    <View>
                      <Text style={[fonts.labelMedium, { color: colors.primary }]}>Lê Quốc Trạng</Text>
                      <View row>
                        {[...Array(4)].map((_, i) => (
                          <AntDesign name='star' color={colors.surfaceVariant} size={14} key={i} />
                        ))}
                        {[...Array(5 - 4)].map((_, i) => (
                          <AntDesign name='staro' color={colors.surfaceVariant} size={14} key={i} />
                        ))}
                      </View>
                    </View>
                    <Button>
                      <Octicons name='report' color={colors.secondary} size={20} />
                    </Button>
                  </View>

                  <View style={{ width: '90%' }}>
                    <Text style={[fonts.bodySmall, { color: colors.primary }]}>{review.content}</Text>
                  </View>
                  <Text style={[fonts.bodySmall, { color: colors.secondary }]}>
                    {dayjs(review.createdAt).format('hh:mm A MM/DD/YYYY')}
                  </Text>
                </View>
              </View>
            </ScrollView>
          </SafeAreaView>
        </SafeAreaProvider>
      </Modal>
    </View>
  )
}
export default Review
