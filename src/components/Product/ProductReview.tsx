import { Divider } from 'react-native-paper'
import { Text, View } from 'react-native-ui-lib'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Button from '../Common/Button'
import Review from './Review'
import { useAppTheme } from '@/hooks/useAppTheme'

const ProductReview = () => {
  const { colors, fonts } = useAppTheme()
  return (
    <>
      <Text style={[fonts.headlineMedium, { color: colors.primary }]}>Đánh giá và nhận xét</Text>
      <View row spread>
        <View row centerV gap-5>
          <AntDesign name='star' color={colors.surfaceVariant} size={20} />
          <Text style={[fonts.headlineMedium, { color: colors.primary }]}>4.5</Text>
          <Text style={[fonts.bodySmall, { color: colors.primary }]}>(20 đánh giá)</Text>
        </View>
        <Button>
          <View row gap-10>
            <Text style={[fonts.headlineSmall, { color: colors.primary }]}>Xem tất cả</Text>
            <AntDesign name='right' size={15} color={colors.primary} />
          </View>
        </Button>
      </View>

      <Divider />
      <Review
        review={{
          content:
            'Đồ đẹp, shop tư vấn nhiệt tình, shop tư vấn nhiệt tình, sẽ, shop tư vấn nhiệt tình, sẽ, sẽ tiếp tục ủng hộ lần sau!',
          createdAt: '2024-03-05T16:39:25.000Z',
          image: 'https://fashionfushion.s3.ap-southeast-1.amazonaws.com/product/Screenshot+2024-03-05+at+23.39.25.png'
        }}
      />
    </>
  )
}

export default ProductReview
