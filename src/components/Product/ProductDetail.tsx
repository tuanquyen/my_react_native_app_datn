import { useAppTheme } from '@/hooks/useAppTheme'
import { Text, View } from 'react-native-ui-lib'

interface ProductDetailProps {
  manufacturer: string
  pattern: string
  style: string
  material: string
  others: { name: string; value: string }[]
}

const ProductDetail = ({ manufacturer, material, others, pattern, style }: ProductDetailProps) => {
  const { colors, fonts } = useAppTheme()
  return (
    <>
      <Text style={[fonts.headlineMedium, { color: colors.primary }]}>Thông tin sản phẩm </Text>
      <View gap-5 style={{ overflow: 'hidden' }}>
        <ProductDetailLine field='Nhà sản xuất' value={manufacturer} />
        <ProductDetailLine field='Mẫu' value={pattern} />
        <ProductDetailLine field='Phong cách' value={style} />
        <ProductDetailLine field='Vật liệu' value={material} />
        {others.map((item, index) => (
          <ProductDetailLine key={index} field={item.name} value={item.value} />
        ))}
      </View>
    </>
  )
}

const ProductDetailLine = ({ field, value }: { field: string; value: string }) => {
  const { colors, fonts } = useAppTheme()
  return (
    <View row gap-5>
      <Text style={[fonts.displaySmall, { color: colors.primary }]}>{field}:</Text>
      <Text style={[fonts.bodySmall, { color: colors.primary, flex: 1 }]}>{value}</Text>
    </View>
  )
}

export default ProductDetail
