import { useAppTheme } from '@/hooks/useAppTheme'
import { FlatList, Image } from 'react-native'
import { Text, View } from 'react-native-ui-lib'
import ItemCard from '../Common/ItemCard'
import { useQuery } from '@tanstack/react-query'
import { getNewProducts } from '@/services/product.service'

const NewProduct = () => {
  const { colors, fonts } = useAppTheme()
  const { data, isLoading } = useQuery({ queryKey: ['newProduct'], queryFn: getNewProducts })
  //   const data = [
  //     { id: '1', title: 'Item 1', priceSale: 259000, price: 300000, isLiked: true },
  //     { id: '2', title: 'Item 2', priceSale: 259000, price: 300000, isLiked: false },
  //     { id: '3', title: 'Item 2', priceSale: 259000, price: 300000, isLiked: false },
  //     { id: '4', title: 'Item 2', priceSale: 259000, price: 300000, isLiked: false },
  //     { id: '5', title: 'Item 2', priceSale: 259000, price: 300000, isLiked: false }
  //   ]
  if (isLoading) {
    return (
      <View>
        <Text>Loading...</Text>
      </View>
    )
  }
  return (
    <View gap-5>
      <Text style={[fonts.headlineLarge, { color: colors.primary }]}>Sản phẩm mới</Text>
      <FlatList
        horizontal
        data={data?.data}
        renderItem={({ item }) => (
          <ItemCard
            id={item.id}
            price={item.variants[0].price_export}
            name={item.name}
            image={item.product_image[0].image}
            isLiked
            priceSale={item.variants[0].price_sale}
          />
        )}
        keyExtractor={(item) => item.id.toString()}
        bounces={false}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{ gap: 20 }}
      />
    </View>
  )
}

export default NewProduct
