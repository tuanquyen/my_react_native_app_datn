import { useAppTheme } from '@/hooks/useAppTheme'
import { Text, View } from 'react-native-ui-lib'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Button from './Common/Button'
import { useEffect } from 'react'

interface QuantitySelectorProps {
  disable?: boolean
  count: number
  setCount: (count: number) => void
  max: number
  min: number
}
const QuantitySelector = ({ disable, count, setCount, max, min }: QuantitySelectorProps) => {
  const { colors, fonts } = useAppTheme()
  const increareCount = () => {
    if (count >= max) return
    setCount(count + 1)
  }
  const decreaseCount = () => {
    if (count <= min) return
    setCount(count - 1)
  }

  useEffect(() => {
    if (count > max) {
      setCount(max)
    }
  }, [max])
  return (
    <View
      row
      spread
      center
      gap-15
      paddingV-5
      height={30}
      paddingH-10
      style={{ borderColor: colors.tertiary, borderWidth: 0.5, borderRadius: 5 }}
    >
      <Button onPress={decreaseCount} disable={disable}>
        <AntDesign name='minus' color={colors.primary} />
      </Button>
      <Text style={[fonts.headlineSmall, { color: colors.primary }]}>{count}</Text>
      <Button onPress={increareCount} disable={disable}>
        <AntDesign name='plus' color={colors.primary} />
      </Button>
    </View>
  )
}

export default QuantitySelector
