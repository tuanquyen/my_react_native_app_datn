import { Color, Variant } from '@/types/app.type'

export function formatNumber(num: number) {
  return num.toLocaleString('de-DE')
}

export function distinct(array: string[]) {
  return [...new Set(array)]
}

export function distinctById(array: Color[]) {
  const unique = array.reduce(
    (result, item) => {
      return result.some((x) => x.id === item.id) ? result : [...result, item]
    },
    [] as { id: number; color_code: string }[]
  )
  return unique
}
export function distinctBySize(array: Variant[]) {
  const unique = array.reduce((result, item) => {
    return result.some((x) => x.size === item.size) ? result : [...result, item]
  }, [] as Variant[])
  return unique
}
