import * as SecureStore from 'expo-secure-store'

export async function setValueStorage(key: string, value: string) {
  await SecureStore.setItemAsync(key, value)
}

export async function getValueStorage(key: string) {
  let result = await SecureStore.getItemAsync(key)
  return result
}
