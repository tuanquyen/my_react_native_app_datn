import { SelectionType } from '@/redux/store'
import { useSelector } from 'react-redux'

const useGetCart = () => {
  const cart = useSelector((state: SelectionType) => state.cart)
  return cart
}
export default useGetCart
