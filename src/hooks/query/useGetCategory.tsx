import { SelectionType } from '@/redux/store'
import { useSelector } from 'react-redux'

const useGetCategories = () => {
  const categories = useSelector((state: SelectionType) => state.categories)
  return {
    loading: categories.status,
    tree: categories.tree,
    root: categories.root,
    leaf: categories.leaf
  }
}

export default useGetCategories
