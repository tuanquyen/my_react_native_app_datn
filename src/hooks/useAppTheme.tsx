import { AppTheme } from '@/themes/themes'
import { useTheme } from 'react-native-paper'

export const useAppTheme = () => {
  return useTheme<AppTheme>()
}
